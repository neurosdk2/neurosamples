import 'package:flutter/material.dart';
import 'package:neurosdk2/neurosdk2.dart';

extension CallibriColorTypeToColor on FCallibriColorType {
  Color toColor() => switch (this) {
        FCallibriColorType.red => Colors.red,
        FCallibriColorType.blue => Colors.blue,
        FCallibriColorType.white => Colors.white,
        FCallibriColorType.yellow => Colors.amber,
        _ => Colors.white,
      };
}
