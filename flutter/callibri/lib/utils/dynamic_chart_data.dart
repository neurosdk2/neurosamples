import 'package:fl_chart/fl_chart.dart';

class DynamicChartData {
  final points = <FlSpot>[];
  final double pointsWindowLength;

  final Stopwatch _stopwatch = Stopwatch();

  DynamicChartData(this.pointsWindowLength);

  void start() {
    _stopwatch.start();
  }

  void add(List<double> samples) {
    final passedTime = _stopwatch.elapsedMilliseconds;
    _stopwatch.reset();

    for (var i = 0; i < points.length; i++) {
      final spot = points[i];
      points[i] = FlSpot(spot.x - passedTime, spot.y);
    }

    points.removeWhere((element) => element.x < 0);

    double startXPosition;
    if (points.isNotEmpty) {
      startXPosition = points.last.x;
    } else {
      startXPosition = pointsWindowLength - passedTime;
    }

    if (samples.length == 1) {
      points.add(FlSpot(pointsWindowLength, samples[0]));
      return;
    }

    double timeStep = (pointsWindowLength - startXPosition) / samples.length;

    int o = 0;
    for (var sample in samples) {
      points.add(FlSpot(startXPosition + o * timeStep, sample));
      o++;
    }
  }
}
