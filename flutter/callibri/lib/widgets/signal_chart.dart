import 'dart:async';

import 'package:filters_sdk/filters_sdk.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:neurosdk2/neurosdk2.dart';
import 'package:neurosdk2_callibri_demo/utils/dynamic_chart_data.dart';
import 'package:neurosdk2_callibri_demo/widgets/filter_selection_dialog.dart';

class SignalChart extends StatefulWidget {
  final Callibri sensor;
  const SignalChart({super.key, required this.sensor});

  @override
  State<SignalChart> createState() => _SignalChartState();
}

class _SignalChartState extends State<SignalChart> {
  static const double windowLength = 2000;
  static const List<double> amplitudeList = [0.01, 0.1, 1];

  final DynamicChartData chartData = DynamicChartData(windowLength);

  final FiltersList flist = FiltersList();

  final List<IIRFilterParam> lpFiltersParam = [];
  final List<IIRFilterParam> hpFiltersParam = [];
  final List<IIRFilterParam> bandStopFiltersParam = [];

  final Map<IIRFilterParam, IIRFilter> selectedFilters = {};

  double currentAmplitude = amplitudeList[0];

  StreamSubscription? signalSubscription;

  @override
  void initState() {
    super.initState();

    signalSubscription = widget.sensor.signalDataStream.listen(processSamples);

    for (var filterParam in preinstalledFilters()) {
      if (filterParam.type case IirFilterType.hp) {
        hpFiltersParam.add(filterParam);
      } else if (filterParam.type case IirFilterType.lp) {
        lpFiltersParam.add(filterParam);
      } else if (filterParam.type case IirFilterType.bandStop) {
        bandStopFiltersParam.add(filterParam);
      }
    }

    hpFiltersParam.sort((a, b) => a.samplingFreq.compareTo(b.samplingFreq));
    lpFiltersParam.sort((a, b) => a.samplingFreq.compareTo(b.samplingFreq));
    bandStopFiltersParam.sort((a, b) => a.samplingFreq.compareTo(b.samplingFreq));

    widget.sensor.execute(FSensorCommand.startSignal);
    chartData.start();
  }

  void processSamples(List<CallibriSignalData> event) {
    List<double> samples = [];
    for (var element in event) {
      samples.addAll(element.samples);
    }

    var filteredSamples = flist.filterArray(samples);
    chartData.add(filteredSamples);
    setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
    signalSubscription?.cancel();

    flist.dispose();
    for (var filter in selectedFilters.values) {
      filter.dispose();
    }

    widget.sensor.execute(FSensorCommand.stopSignal);
  }

  LineChartBarData signalLine(List<FlSpot> points) {
    final color = Theme.of(context).colorScheme.secondary;

    return LineChartBarData(
      spots: points,
      show: points.isNotEmpty,
      dotData: const FlDotData(show: false),
      color: color,
      barWidth: 2,
      isCurved: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Signal Graph",
          style: TextStyle(
            fontSize: 20,
            color: Theme.of(context).colorScheme.primary,
          ),
        ),
        SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Filters:",
              style: TextStyle(
                fontSize: 15,
                color: Theme.of(context).colorScheme.secondary,
              ),
            ),
            OutlinedButton(
              onPressed: () => showFilterSelectPopup("LP", lpFiltersParam),
              child: Text("LP"),
            ),
            OutlinedButton(
              onPressed: () => showFilterSelectPopup("LP", hpFiltersParam),
              child: Text("HP"),
            ),
            OutlinedButton(
              onPressed: () => showFilterSelectPopup("LP", bandStopFiltersParam),
              child: Text("BandStop"),
            ),
          ],
        ),
        SizedBox(height: 10),
        Row(
          children: [
            Text(
              "Amplitude (µV):",
              style: TextStyle(
                fontSize: 15,
                color: Theme.of(context).colorScheme.secondary,
              ),
            ),
            const Spacer(),
            SegmentedButton<double>(
              showSelectedIcon: false,
              style: SegmentedButton.styleFrom(
                backgroundColor: Colors.transparent,
                foregroundColor: Theme.of(context).colorScheme.secondary,
                selectedForegroundColor: Colors.white,
                selectedBackgroundColor: Theme.of(context).colorScheme.primary,
                visualDensity: const VisualDensity(horizontal: -3, vertical: -3),
              ),
              segments: <ButtonSegment<double>>[
                for (final value in amplitudeList)
                  ButtonSegment<double>(
                    value: value,
                    label: Text(value.toString()),
                  ),
              ],
              selected: <double>{currentAmplitude},
              onSelectionChanged: (amplitude) => setState(() => currentAmplitude = amplitude.first),
            ),
          ],
        ),
        SizedBox(height: 10),
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            'y: ${chartData.points.isNotEmpty ? chartData.points.last.y : "-"}',
            style: TextStyle(fontSize: 15, color: Theme.of(context).colorScheme.secondary),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(bottom: 24.0),
            child: LineChart(
              LineChartData(
                maxY: currentAmplitude,
                minY: -currentAmplitude,
                minX: 0,
                maxX: windowLength,
                lineTouchData: const LineTouchData(enabled: false),
                clipData: const FlClipData.all(),
                gridData: const FlGridData(
                  show: true,
                  drawVerticalLine: false,
                  drawHorizontalLine: false,
                ),
                borderData: FlBorderData(show: true, border: Border.all(color: Theme.of(context).colorScheme.secondary)),
                lineBarsData: [signalLine(chartData.points)],
                titlesData: const FlTitlesData(show: false),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Future showFilterSelectPopup(String typeName, List<IIRFilterParam> filters) async {
    await showDialog(
      context: context,
      builder: (context) => FilterSelectionDialog(
        filters: filters,
        typeName: typeName,
        selectedFilters: selectedFilters,
        onFilterSelected: onFilterSelected,
      ),
    );
  }

  void onFilterSelected(IIRFilterParam filterParam) {
    if (selectedFilters[filterParam] case IIRFilter filter) {
      selectedFilters.remove(filterParam);
      flist.deleteFilter(filter);
      filter.dispose();
      return;
    }

    final newFilter = IIRFilter.createWithParams(filterParam);
    selectedFilters[filterParam] = newFilter;
    flist.addFilter(newFilter);
  }
}
