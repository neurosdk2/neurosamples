import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:neurosdk2/neurosdk2.dart';

class FoundDeviceListElement extends StatefulWidget {
  final FSensorInfo sensorInfo;
  final Scanner scanner;

  final void Function(FSensorInfo)? onTap;

  const FoundDeviceListElement(
      {super.key, required this.sensorInfo, this.onTap, required this.scanner});

  @override
  State<FoundDeviceListElement> createState() => _FoundDeviceListElementState();
}

class _FoundDeviceListElementState extends State<FoundDeviceListElement> {
  bool isError = false;
  bool isInited = false;
  LoadedSensorInfo? loadedInfo;
  CancelableCompleter<Sensor?>? _connectionCompleter;

  @override
  void initState() {
    super.initState();
    if (isInited) return;
    isInited = true;
    _asyncInit();
  }

  @override
  void didUpdateWidget(covariant FoundDeviceListElement oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.sensorInfo.address != widget.sensorInfo.address) _asyncInit();
  }

  @override
  void dispose() {
    super.dispose();
    _connectionCompleter?.operation.cancel();
  }

  Future _asyncInit() async {
    if (mounted) setState(() => loadedInfo = null);

    Sensor? sensor;
    _connectionCompleter?.operation.cancel();

    try {
      _connectionCompleter = CancelableCompleter(onCancel: () => null);
      _connectionCompleter!.complete(
        widget.scanner.createSensor(widget.sensorInfo),
      );

      sensor = await _connectionCompleter!.operation.valueOrCancellation();

      if (sensor == null) {
        if (mounted) setState(() => isError = true);
        return;
      }

      loadedInfo = LoadedSensorInfo(
        await sensor.serialNumber.value,
        await sensor.batteryPower.value,
      );

      if (mounted) setState(() {});
    } on Exception {
      if (mounted) setState(() => isError = true);
    } finally {
      sensor?.dispose();
    }
  }

  @override
  Widget build(BuildContext context) {
    if (isError) {
      return Card(
        child: ListTile(
          enabled: false,
          title: Text(widget.sensorInfo.name),
          subtitle: Text(widget.sensorInfo.address),
          leading: const Icon(Icons.close_rounded, color: Colors.red),
        ),
      );
    }

    if (loadedInfo == null) {
      return Card(
        child: ListTile(
          enabled: false,
          title: Text(widget.sensorInfo.name),
          subtitle: Text(widget.sensorInfo.address),
          trailing: const SizedBox(
            height: 20,
            width: 20,
            child: CircularProgressIndicator(),
          ),
        ),
      );
    }

    return Card(
      child: ListTile(
        onTap: () => widget.onTap?.call(widget.sensorInfo),
        title: Text("${widget.sensorInfo.name} : ${loadedInfo!.serialNumber}"),
        subtitle: Text(widget.sensorInfo.address),
        trailing: Text("${loadedInfo!.battery}%"),
      ),
    );
  }
}

class LoadedSensorInfo {
  final String serialNumber;
  final int battery;

  LoadedSensorInfo(this.serialNumber, this.battery);
}
