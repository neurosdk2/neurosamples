import 'package:flutter/material.dart';

class CallibriCardWithElectrodes extends StatelessWidget {
  final Color color;
  final Color electrodeColor;

  const CallibriCardWithElectrodes({
    super.key,
    required this.color,
    required this.electrodeColor,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Material(
            elevation: 1,
            color: Theme.of(context).cardColor,
            shadowColor: Theme.of(context).shadowColor,
            borderRadius: BorderRadius.circular(50),
            child: Padding(
              padding: const EdgeInsets.all(1.0),
              child: Material(
                elevation: 1,
                color: Theme.of(context).cardColor,
                shadowColor: Theme.of(context).shadowColor,
                borderRadius: BorderRadius.circular(50),
                child: Padding(
                  padding: const EdgeInsets.all(1.0),
                  child: Image.asset(
                    'assets/images/callibri.png',
                    color: color,
                    colorBlendMode: BlendMode.srcIn,
                  ),
                ),
              ),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: 15,
                height: 15,
                child: Card(color: electrodeColor),
              ),
              SizedBox(
                height: 15,
                width: 15,
                child: Card(color: electrodeColor),
              )
            ],
          )
        ],
      ),
    );
  }
}
