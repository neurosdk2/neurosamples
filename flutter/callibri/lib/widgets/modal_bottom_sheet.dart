import 'package:flutter/material.dart';

class ModalBottomSheet extends StatelessWidget {
  final Widget child;
  final double? height;

  const ModalBottomSheet({super.key, required this.child, this.height});

  @override
  Widget build(BuildContext context) {
    return SizedBox(height: height, child: child);
  }
}
