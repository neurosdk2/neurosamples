import 'dart:async';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:neurosdk2/neurosdk2.dart';
import 'package:neurosdk2_callibri_demo/utils/dynamic_chart_data.dart';

class EnvelopeChart extends StatefulWidget {
  final Callibri sensor;
  const EnvelopeChart({super.key, required this.sensor});

  @override
  State<EnvelopeChart> createState() => _EnvelopeChartState();
}

class _EnvelopeChartState extends State<EnvelopeChart> {
  static const double windowLength = 2000;
  static const List<double> amplitudeList = [0.01, 0.1, 1];

  final DynamicChartData chartData = DynamicChartData(windowLength);
  double currentAmplitude = amplitudeList[0];

  StreamSubscription? envelopeSubscription;

  @override
  void initState() {
    super.initState();

    envelopeSubscription = widget.sensor.envelopeDataStream.listen(
      (event) {
        chartData.add(event.map((e) => e.sample).toList());
        setState(() {});
      },
    );

    widget.sensor.execute(FSensorCommand.startEnvelope);

    chartData.start();
  }

  @override
  void dispose() {
    super.dispose();
    envelopeSubscription?.cancel();
    widget.sensor.execute(FSensorCommand.stopEnvelope);
  }

  LineChartBarData signalLine(List<FlSpot> points) {
    final color = Theme.of(context).colorScheme.secondary;

    return LineChartBarData(
      spots: points,
      show: points.isNotEmpty,
      dotData: const FlDotData(
        show: false,
      ),
      color: color,
      barWidth: 2,
      isCurved: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Envelope Graph",
          style: TextStyle(
            fontSize: 20,
            color: Theme.of(context).colorScheme.primary,
          ),
        ),
        const SizedBox(height: 20),
        Row(
          children: [
            Text(
              "Amplitude (µV):",
              style: TextStyle(
                fontSize: 15,
                color: Theme.of(context).colorScheme.secondary,
              ),
            ),
            const Spacer(),
            SegmentedButton<double>(
              showSelectedIcon: false,
              style: SegmentedButton.styleFrom(
                backgroundColor: Colors.transparent,
                foregroundColor: Theme.of(context).colorScheme.secondary,
                selectedForegroundColor: Colors.white,
                selectedBackgroundColor: Theme.of(context).colorScheme.primary,
                visualDensity:
                    const VisualDensity(horizontal: -3, vertical: -3),
              ),
              segments: <ButtonSegment<double>>[
                for (final value in amplitudeList)
                  ButtonSegment<double>(
                    value: value,
                    label: Text(value.toString()),
                  ),
              ],
              selected: <double>{currentAmplitude},
              onSelectionChanged: (amplitude) =>
                  setState(() => currentAmplitude = amplitude.first),
            ),
          ],
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            'y: ${chartData.points.isNotEmpty ? chartData.points.last.y : "-"}',
            style: TextStyle(
              fontSize: 15,
              color: Theme.of(context).colorScheme.secondary,
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(bottom: 24.0),
            child: LineChart(
              LineChartData(
                maxY: currentAmplitude,
                minY: 0,
                minX: 0,
                maxX: windowLength,
                lineTouchData: const LineTouchData(enabled: false),
                clipData: const FlClipData.all(),
                gridData: const FlGridData(
                  show: true,
                  drawVerticalLine: false,
                  drawHorizontalLine: false,
                ),
                borderData: FlBorderData(
                    show: true,
                    border: Border.all(
                        color: Theme.of(context).colorScheme.secondary)),
                lineBarsData: [
                  signalLine(chartData.points),
                ],
                titlesData: const FlTitlesData(
                  show: false,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
