import 'package:flutter/material.dart';

ThemeData buildTheme() => ThemeData(
      // colorSchemeSeed: const Color(0xff6750a4),
      colorSchemeSeed: const Color.fromARGB(255, 41, 145, 194),
      useMaterial3: true,
    );
