import 'dart:async';
import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:geolocator/geolocator.dart';
import 'package:neurosdk2/neurosdk2.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:neurosdk2_callibri_demo/screens/callibri_screen/callibri_screen.dart';
import 'package:neurosdk2_callibri_demo/widgets/device_search.dart';
import 'package:neurosdk2_callibri_demo/widgets/modal_bottom_sheet.dart';

class StartScreenBody extends StatefulWidget {
  const StartScreenBody({super.key});

  @override
  State<StartScreenBody> createState() => _StartScreenBodyState();
}

class _StartScreenBodyState extends State<StartScreenBody> {
  Scanner? _scanner;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ElevatedButton(
        onPressed: _showSearchDeviceBottomSheet,
        child: const Text("Search Devices"),
      ),
    );
  }

  void _showSearchDeviceBottomSheet() {
    showModalBottomSheet(
      context: context,
      showDragHandle: true,
      builder: (context) => ModalBottomSheet(
        height: 400,
        child: FutureBuilder<Scanner?>(
          future: _createScanner(),
          builder: (context, snapshot) {
            if (!snapshot.hasData || snapshot.data == null) {
              return const Center(child: CircularProgressIndicator());
            }

            return DeviceSearch(
              scanner: snapshot.data!,
              deviceSelected: _onDeviceSelected,
            );
          },
        ),
      ),
    );
  }

  Future<Scanner?> _createScanner() async {
    if (!await _checks()) return null;

    if (_scanner == null) {
      try {
        _scanner = await Scanner.create([FSensorFamily.leCallibri]);
      } on Exception catch (e) {
        if (kDebugMode) {
          print(e);
        }

        return null;
      }
    }

    return _scanner;
  }

  Future _onDeviceSelected(FSensorInfo info) async {
    if (_scanner == null) return;
    if (!mounted) return;

    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => const PopScope(
        canPop: false,
        child: AlertDialog(
          content: Row(
            children: [
              CircularProgressIndicator(),
              SizedBox(width: 20),
              Text("Connecting ..."),
            ],
          ),
        ),
      ),
    );

    Sensor? device;
    try {
      device = await _scanner!.createSensor(info);
      if (device case final Callibri callibri) {
        Navigator.pop(context);

        await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => SensorScreen(callibri: callibri),
          ),
        );
      } else {
        Navigator.pop(context);
        return;
      }
    } on Exception catch (e) {
      if (kDebugMode) {
        print(e);
      }

      Navigator.pop(context);
      return;
    } finally {
      device?.dispose();
    }
  }

  Future<bool> _checks() async {
    final notGrantedPermissions = await _checkPermissions();

    if (notGrantedPermissions.isNotEmpty) {
      _showDialog("Not all permission granted!");
      return false;
    }

    return await _checkBluetooth() && await _checkGPS();
  }

  Future<List<Permission>> _checkPermissions() async {
    if (Platform.isIOS || Platform.isWindows || Platform.isMacOS) {
      return List.empty();
    }

    final statuses = await [
      Permission.bluetoothConnect,
      Permission.bluetoothScan,
      Permission.location,
    ].request();

    // if all permissions were granted
    if (statuses.values.every((s) => s.isGranted)) {
      return List.empty();
    }

    return statuses.keys.where((k) => !statuses[k]!.isGranted).toList();
  }

  Future<bool> _checkBluetooth() async {
    if (Platform.isWindows) return true;
    if (await FlutterBluePlus.isSupported == false) {
      _showDialog("Bluetooth not supported by this device!");
      return false;
    }

    if (await FlutterBluePlus.adapterState.first != BluetoothAdapterState.on) {
      _showDialog("Bluetooth must be turned on!");
      return false;
    }

    return true;
  }

  Future<bool> _checkGPS() async {
    if (!Platform.isAndroid) return true;

    var androidInfo = await DeviceInfoPlugin().androidInfo;
    if (androidInfo.version.sdkInt >= 32) return true;

    if (!await Geolocator.isLocationServiceEnabled()) {
      _showDialog("Location Service must be turned on!");
      return false;
    }

    return true;
  }

  Future _showDialog(String message) async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => AlertDialog(
        content: Text(message),
        actions: [
          ElevatedButton(
            onPressed: () => Navigator.pop(context),
            child: const Text("OK"),
          )
        ],
      ),
    );
  }
}
