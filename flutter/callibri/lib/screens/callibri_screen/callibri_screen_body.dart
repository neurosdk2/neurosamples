import 'package:flutter/material.dart';
import 'package:neurosdk2/neurosdk2.dart';
import 'package:neurosdk2_callibri_demo/utils/callibri_color_type_to_color.dart';
import 'package:neurosdk2_callibri_demo/widgets/callibri_card_with_electrodes.dart';
import 'package:neurosdk2_callibri_demo/widgets/ecg_example.dart';
import 'package:neurosdk2_callibri_demo/widgets/envelope_chart.dart';
import 'package:neurosdk2_callibri_demo/widgets/modal_bottom_sheet.dart';
import 'package:neurosdk2_callibri_demo/widgets/signal_chart.dart';
import 'package:neurosdk2_callibri_demo/widgets/spectrum_chart.dart';

class SensorScreenBody extends StatefulWidget {
  final Callibri callibri;

  const SensorScreenBody({
    super.key,
    required this.callibri,
  });

  @override
  State<SensorScreenBody> createState() => _SensorScreenBodyState();
}

class _SensorScreenBodyState extends State<SensorScreenBody> {
  late final CachedSensorInfo cachedInfo;
  bool isLoading = true;
  bool isError = false;

  @override
  void initState() {
    super.initState();
    asyncInit();
  }

  Future asyncInit() async {
    try {
      cachedInfo = CachedSensorInfo(
        serialNumber: await widget.callibri.serialNumber.value,
        battery: await widget.callibri.batteryPower.value,
        color: await widget.callibri.color.value,
        state: await widget.callibri.state.value,
        name: await widget.callibri.name.value,
      );

      isLoading = false;
      setState(() {});
    } on Exception catch (e) {
      isError = true;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    if (isLoading || isError) {
      return const SizedBox();
    }

    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    StreamBuilder<FCallibriElectrodeState>(
                      stream: widget.callibri.electrodeStateStream,
                      builder: (context, snapshot) => CallibriCardWithElectrodes(
                        color: cachedInfo.color.toColor(),
                        electrodeColor: snapshot.hasData && snapshot.data != FCallibriElectrodeState.elStDetached ? Colors.green : Colors.grey,
                      ),
                    ),
                    const SizedBox(width: 10),
                    Text(
                      "${cachedInfo.name.replaceAll("_", " ")} : ${cachedInfo.serialNumber}",
                      style: const TextStyle(fontSize: 16),
                    ),
                    const Spacer(),
                    StreamBuilder<int>(
                      stream: widget.callibri.batteryPowerStream,
                      builder: (context, snapshot) => Text(
                        snapshot.hasData ? "${snapshot.data} %" : "${cachedInfo.battery} %",
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                buildSignalButton("Signal", () => SignalChart(sensor: widget.callibri)),
                const SizedBox(width: 10),
                buildEnvelopeButton("Envelope", () => EnvelopeChart(sensor: widget.callibri)),
              ],
            ),
            const SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                buildSignalButton("Spectrum", () => SpectrumChart(sensor: widget.callibri)),
                const SizedBox(width: 10),
                buildSignalButton("ECG", () => EcgExample(sensor: widget.callibri)),
              ],
            ),
            const SizedBox(height: 20),
            Card(
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    buildInfoLine(
                      "Sensor Family:",
                      FutureBuilder<FSensorFamily>(
                        future: widget.callibri.sensFamily.value,
                        builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!.name) : const SizedBox(),
                      ),
                    ),
                    buildInfoLine(
                      "Serial Number:",
                      FutureBuilder<String>(
                        future: widget.callibri.serialNumber.value,
                        builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!) : const SizedBox(),
                      ),
                    ),
                    buildInfoLine(
                      "Address:",
                      FutureBuilder<String>(
                        future: widget.callibri.address.value,
                        builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!) : const SizedBox(),
                      ),
                    ),
                    buildInfoLine(
                      "Sensor Version:",
                      FutureBuilder<FSensorVersion>(
                        future: widget.callibri.version.value,
                        builder: (context, snapshot) => snapshot.hasData
                            ? Text(
                                "[FW]: ${snapshot.data!.fwMajor}.${snapshot.data!.fwMinor}.${snapshot.data!.fwPatch}\n"
                                "[HW]: ${snapshot.data!.hwMajor}.${snapshot.data!.hwMinor}.${snapshot.data!.hwPatch}\n"
                                "[Ext]: ${snapshot.data!.extMajor}",
                                textAlign: TextAlign.end,
                              )
                            : const SizedBox(),
                      ),
                    ),
                    buildInfoLine(
                      "Electrode state:",
                      StreamBuilder<FCallibriElectrodeState>(
                        stream: widget.callibri.electrodeStateStream,
                        builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!.name) : const SizedBox(),
                      ),
                    ),
                    buildInfoLine(
                      "State:",
                      StreamBuilder<FSensorState>(
                        stream: widget.callibri.sensorStateStream,
                        builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!.name) : Text(cachedInfo.state.name),
                      ),
                    ),
                    buildInfoLine(
                      "Chanels:",
                      FutureBuilder<int>(
                        future: widget.callibri.channelsCount.value,
                        builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!.toString()) : const SizedBox(),
                      ),
                    ),
                    buildInfoLine(
                      "Data offset:",
                      FutureBuilder<FSensorDataOffset>(
                        future: widget.callibri.dataOffset.value,
                        builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!.name) : const SizedBox(),
                      ),
                    ),
                    buildInfoLine(
                      "External Switch Input:",
                      FutureBuilder<FSensorExternalSwitchInput>(
                        future: widget.callibri.extSwInput.value,
                        builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!.name) : const SizedBox(),
                      ),
                    ),
                    buildInfoLine(
                      "Firmware Mode:",
                      FutureBuilder<FSensorFirmwareMode>(
                        future: widget.callibri.firmwareMode.value,
                        builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!.name) : const SizedBox(),
                      ),
                    ),
                    buildInfoLine(
                      "Gain:",
                      FutureBuilder<FSensorGain>(
                        future: widget.callibri.gain.value,
                        builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!.name) : const SizedBox(),
                      ),
                    ),
                    buildInfoLine(
                      "Sampling Frequency:",
                      FutureBuilder<FSensorSamplingFrequency>(
                        future: widget.callibri.samplingFrequency.value,
                        builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!.name) : const SizedBox(),
                      ),
                    ),
                    buildInfoLine(
                      "Envelope Sampling Frequency:",
                      FutureBuilder<FSensorSamplingFrequency>(
                        future: widget.callibri.samplingFrequencyEnvelope.value,
                        builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!.name) : const SizedBox(),
                      ),
                    ),
                    buildInfoLine(
                      "Resp Sampling Frequency:",
                      FutureBuilder<FSensorSamplingFrequency>(
                        future: widget.callibri.samplingFrequencyResp.value,
                        builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!.name) : const SizedBox(),
                      ),
                    ),
                    buildInfoLine(
                      "MEMS Sampling Frequency:",
                      FutureBuilder<FSensorSamplingFrequency>(
                        future: widget.callibri.samplingFrequencyMEMS.value,
                        builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!.name) : const SizedBox(),
                      ),
                    ),
                    buildInfoLine(
                      "Signal Type:",
                      FutureBuilder<CallibriSignalType>(
                        future: widget.callibri.signalType.value,
                        builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!.name) : const SizedBox(),
                      ),
                    ),
                    buildInfoLine(
                      "Accelerometer Sensitivity:",
                      FutureBuilder<FSensorAccelerometerSensitivity>(
                        future: widget.callibri.accSens.value,
                        builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!.name) : const SizedBox(),
                      ),
                    ),
                    buildInfoLine(
                      "Gyro Sensitivity:",
                      FutureBuilder<FSensorGyroscopeSensitivity>(
                        future: widget.callibri.gyroSens.value,
                        builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!.name) : const SizedBox(),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            buildInfoCard(
              "Supported Features:",
              FutureBuilder<Set<FSensorFeature>>(
                future: widget.callibri.features.value,
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return const SizedBox();
                  }

                  final features = snapshot.data!;

                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [for (final feature in features) Text(feature.name)],
                  );
                },
              ),
            ),
            buildInfoCard(
              "Supported Commands:",
              FutureBuilder<Set<FSensorCommand>>(
                future: widget.callibri.commands.value,
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return const SizedBox();
                  }

                  final commands = snapshot.data!;

                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [for (final command in commands) Text(command.name)],
                  );
                },
              ),
            ),
            buildInfoCard(
              "Supported Parameters:",
              FutureBuilder<List<FParameterInfo?>>(
                future: widget.callibri.parameters.value,
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return const SizedBox();
                  }

                  final params = snapshot.data!;

                  return Column(
                    children: [
                      for (final param in params)
                        Row(
                          children: [
                            Text(param!.param.name),
                            const Spacer(),
                            Text(param.paramAccess.name),
                          ],
                        ),
                    ],
                  );
                },
              ),
            ),
            buildInfoCard(
              "Supported Filters:",
              FutureBuilder<Set<FSensorFilter>>(
                future: widget.callibri.supportedFilters.value,
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return const SizedBox();
                  }

                  final filters = snapshot.data!;

                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      for (final filter in filters) Text(filter.name),
                    ],
                  );
                },
              ),
            ),
            buildInfoCard(
              "Hardware Filters:",
              FutureBuilder<Set<FSensorFilter>>(
                future: widget.callibri.hardwareFilters.value,
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return const SizedBox();
                  }

                  final filters = snapshot.data!;

                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      for (final filter in filters) Text(filter.name),
                    ],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildInfoLine(String title, Widget data) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [Text(title), const Spacer(), data],
    );
  }

  Widget buildInfoCard(String title, Widget data) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              title,
              style: const TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            ),
            data,
          ],
        ),
      ),
    );
  }

  Widget buildSignalButton(String text, Widget Function() buildPopupBody) {
    return SizedBox(
      width: 120,
      child: FutureBuilder(
        future: widget.callibri.isSupportedFeature(FSensorFeature.signal),
        builder: (context, snapshot) => buildPopupButton(text, snapshot.data ?? false, buildPopupBody),
      ),
    );
  }

  Widget buildEnvelopeButton(String text, Widget Function() buildPopupBody) {
    return SizedBox(
      width: 120,
      child: FutureBuilder(
        future: widget.callibri.isSupportedFeature(FSensorFeature.envelope),
        builder: (context, snapshot) => buildPopupButton(text, snapshot.data ?? false, buildPopupBody),
      ),
    );
  }

  Widget buildPopupButton(String text, bool isActive, Widget Function() buildPopupBody) {
    return ElevatedButton(
      onPressed: isActive
          ? () async {
              showModalBottomSheet(
                context: context,
                showDragHandle: true,
                backgroundColor: const Color.fromARGB(255, 26, 26, 26),
                useSafeArea: true,
                isScrollControlled: true,
                builder: (context) => FractionallySizedBox(
                  heightFactor: 0.8,
                  widthFactor: 1,
                  child: ModalBottomSheet(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: buildPopupBody(),
                    ),
                  ),
                ),
              );
            }
          : null,
      child: Text(text),
    );
  }
}

class CachedSensorInfo {
  final String name;
  final String serialNumber;
  final FCallibriColorType color;
  final int battery;
  final FSensorState state;

  CachedSensorInfo({
    required this.battery,
    required this.serialNumber,
    required this.color,
    required this.state,
    required this.name,
  });
}
