import 'package:flutter/material.dart';
import 'package:neurosdk2/callibri_sensor.dart';
import 'package:neurosdk2_callibri_demo/screens/callibri_screen/callibri_screen_body.dart';

class SensorScreen extends StatelessWidget {
  final Callibri callibri;

  const SensorScreen({super.key, required this.callibri});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SensorScreenBody(callibri: callibri),
    );
  }
}
