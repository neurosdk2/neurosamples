import 'package:flutter/material.dart';
import 'package:neurosdk2/neurosdk2.dart';
import 'package:neurosdk2_brainbit_demo/widgets/brainbit_elecrode.dart';
import 'package:neurosdk2_brainbit_demo/widgets/modal_bottom_sheet.dart';
import 'package:neurosdk2_brainbit_demo/widgets/signal_chart.dart';
import 'package:neurosdk2_brainbit_demo/widgets/spectrum_chart.dart';

class BrainbitScreenBody extends StatefulWidget {
  final BrainBit brainbit;

  const BrainbitScreenBody({super.key, required this.brainbit});

  @override
  State<BrainbitScreenBody> createState() => _BrainbitScreenBodyState();
}

class _BrainbitScreenBodyState extends State<BrainbitScreenBody> {
  late final CachedSensorInfo cachedInfo;
  bool isResistEnabled = false;
  bool isLoading = true;
  bool isError = false;

  @override
  void initState() {
    super.initState();
    asyncInit();
  }

  Future asyncInit() async {
    try {
      cachedInfo = CachedSensorInfo(
        serialNumber: await widget.brainbit.serialNumber.value,
        battery: await widget.brainbit.batteryPower.value,
        state: await widget.brainbit.state.value,
        name: await widget.brainbit.name.value,
      );

      isLoading = false;
      setState(() {});
    } on Exception catch (e) {
      isError = true;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    if (isLoading || isError) {
      return const SizedBox();
    }

    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Text(
                      "${cachedInfo.name.replaceAll("_", " ")} : ${cachedInfo.serialNumber}",
                      style: const TextStyle(fontSize: 16),
                    ),
                    const Spacer(),
                    StreamBuilder<int>(
                      stream: widget.brainbit.batteryPowerStream,
                      builder: (context, snapshot) => Text(
                        snapshot.hasData ? "${snapshot.data} %" : "${cachedInfo.battery} %",
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                buildSignalButton("Signal", () => SignalChart(sensor: widget.brainbit)),
                const SizedBox(width: 10),
                buildSignalButton("Spectrum", () => SpectrumChart(sensor: widget.brainbit)),
              ],
            ),
            const SizedBox(height: 20),
            buildInfoCard(
              "Electrode Resistance:",
              Column(
                children: [
                  Card(
                    elevation: 10,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: StreamBuilder<BrainBitResistData>(
                          stream: widget.brainbit.resistDataStream,
                          builder: (context, snapshot) {
                            Color t4;
                            Color t3;
                            Color o1;
                            Color o2;

                            if (!snapshot.hasData) {
                              o1 = Colors.grey;
                              o2 = Colors.grey;
                              t3 = Colors.grey;
                              t4 = Colors.grey;
                            } else {
                              final data = snapshot.data!;
                              check(double v) => !v.isNaN && v != double.infinity;

                              o1 = check(data.o1) ? Colors.green : Colors.grey;
                              o2 = check(data.o2) ? Colors.green : Colors.grey;
                              t3 = check(data.t3) ? Colors.green : Colors.grey;
                              t4 = check(data.t3) ? Colors.green : Colors.grey;
                            }

                            return Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                BrainbitElectrode(color: o1, name: "O1"),
                                BrainbitElectrode(color: t3, name: "T3"),
                                BrainbitElectrode(color: t4, name: "T4"),
                                BrainbitElectrode(color: o2, name: "O2"),
                              ],
                            );
                          }),
                    ),
                  ),
                  ElevatedButton(
                    onPressed: !isResistEnabled
                        ? () {
                            widget.brainbit.execute(SensorCommand.startResist);
                            setState(() => isResistEnabled = true);
                          }
                        : () {
                            widget.brainbit.execute(SensorCommand.stopResist);
                            setState(() => isResistEnabled = false);
                          },
                    child: !isResistEnabled ? const Text("Start") : const Text("Stop"),
                  ),
                ],
              ),
            ),
            Card(
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    buildInfoLine(
                        "Sensor Family:",
                        FutureBuilder<SensorFamily>(
                            future: widget.brainbit.sensFamily.value, builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!.name) : const SizedBox())),
                    buildInfoLine(
                        "Serial Number:",
                        FutureBuilder<String>(
                            future: widget.brainbit.serialNumber.value, builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!) : const SizedBox())),
                    buildInfoLine("Address:",
                        FutureBuilder<String>(future: widget.brainbit.address.value, builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!) : const SizedBox())),
                    buildInfoLine(
                        "Sensor Version:",
                        FutureBuilder<SensorVersion>(
                            future: widget.brainbit.version.value,
                            builder: (context, snapshot) => snapshot.hasData
                                ? Text(
                                    "[FW]: ${snapshot.data!.fwMajor}.${snapshot.data!.fwMinor}.${snapshot.data!.fwPatch}\n"
                                    "[HW]: ${snapshot.data!.hwMajor}.${snapshot.data!.hwMinor}.${snapshot.data!.hwPatch}\n"
                                    "[Ext]: ${snapshot.data!.extMajor}",
                                    textAlign: TextAlign.end,
                                  )
                                : const SizedBox())),
                    buildInfoLine(
                        "State:",
                        StreamBuilder<SensorState>(
                            stream: widget.brainbit.sensorStateStream,
                            builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!.name) : Text(cachedInfo!.state.name))),
                    buildInfoLine(
                        "Chanels:",
                        FutureBuilder<int>(
                            future: widget.brainbit.channelsCount.value, builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!.toString()) : const SizedBox())),
                    buildInfoLine(
                        "Data offset:",
                        FutureBuilder<SensorDataOffset>(
                            future: widget.brainbit.dataOffset.value, builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!.name) : const SizedBox())),
                    buildInfoLine(
                        "Firmware Mode:",
                        FutureBuilder<SensorFirmwareMode>(
                            future: widget.brainbit.firmwareMode.value, builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!.name) : const SizedBox())),
                    buildInfoLine(
                        "Gain:",
                        FutureBuilder<SensorGain>(
                            future: widget.brainbit.gain.value, builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!.name) : const SizedBox())),
                    buildInfoLine(
                        "Sampling Frequency:",
                        FutureBuilder<SensorSamplingFrequency>(
                            future: widget.brainbit.samplingFrequency.value, builder: (context, snapshot) => snapshot.hasData ? Text(snapshot.data!.name) : const SizedBox())),
                  ],
                ),
              ),
            ),
            buildInfoCard(
              "Supported Features:",
              FutureBuilder<Set<SensorFeature>>(
                  future: widget.brainbit.features.value,
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return const SizedBox();
                    }

                    final features = snapshot.data!;

                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [for (final feature in features) Text(feature.name)],
                    );
                  }),
            ),
            buildInfoCard(
              "Supported Commands:",
              FutureBuilder<Set<SensorCommand>>(
                  future: widget.brainbit.commands.value,
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return const SizedBox();
                    }

                    final commands = snapshot.data!;

                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [for (final command in commands) Text(command.name)],
                    );
                  }),
            ),
            buildInfoCard(
              "Supported Parameters:",
              FutureBuilder<List<ParameterInfo?>>(
                  future: widget.brainbit.parameters.value,
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return const SizedBox();
                    }

                    final params = snapshot.data!;

                    return Column(
                      children: [
                        for (final param in params)
                          Row(
                            children: [
                              Text(param!.param.name),
                              const Spacer(),
                              Text(param.paramAccess.name),
                            ],
                          ),
                      ],
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildInfoLine(String title, Widget data) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [Text(title), const Spacer(), data],
    );
  }

  Widget buildInfoCard(String title, Widget data) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              title,
              style: const TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            ),
            data,
          ],
        ),
      ),
    );
  }

  Widget buildSignalButton(String text, Widget Function() buildBody) {
    return SizedBox(
      width: 120,
      child: FutureBuilder(
        future: widget.brainbit.isSupportedFeature(SensorFeature.signal),
        builder: (context, snapshot) {
          final bool hasSignal = snapshot.data ?? false;

          return ElevatedButton(
            onPressed: hasSignal
                ? () async {
                    await widget.brainbit.execute(SensorCommand.stopResist);
                    setState(() => isResistEnabled = false);

                    showModalBottomSheet(
                      context: context,
                      showDragHandle: true,
                      backgroundColor: const Color.fromARGB(255, 26, 26, 26),
                      useSafeArea: true,
                      isScrollControlled: true,
                      builder: (context) => FractionallySizedBox(
                        heightFactor: 0.8,
                        child: ModalBottomSheet(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: buildBody(),
                          ),
                        ),
                      ),
                    );
                  }
                : null,
            child: Text(text),
          );
        },
      ),
    );
  }
}

class CachedSensorInfo {
  final String name;
  final int battery;
  final SensorState state;
  final String serialNumber;

  CachedSensorInfo({
    required this.battery,
    required this.serialNumber,
    required this.state,
    required this.name,
  });
}
