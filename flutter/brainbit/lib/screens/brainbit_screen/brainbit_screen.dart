import 'package:flutter/material.dart';
import 'package:neurosdk2/neurosdk2.dart';
import 'package:neurosdk2_brainbit_demo/screens/brainbit_screen/brainbit_screen_body.dart';

class BrainbitScreen extends StatelessWidget {
  final BrainBit brainbit;

  const BrainbitScreen({super.key, required this.brainbit});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: BrainbitScreenBody(brainbit: brainbit),
    );
  }
}
