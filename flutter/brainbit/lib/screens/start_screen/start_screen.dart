import 'package:flutter/material.dart';
import 'package:neurosdk2_brainbit_demo/screens/start_screen/start_screen_body.dart';

class StartScreen extends StatelessWidget {
  const StartScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(), body: const StartScreenBody());
  }
}
