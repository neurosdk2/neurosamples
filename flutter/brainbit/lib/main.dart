import 'package:flutter/material.dart';
import 'package:neurosdk2_brainbit_demo/screens/start_screen/start_screen.dart';
import 'package:neurosdk2_brainbit_demo/themes/theme.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: buildTheme(),
      home: const StartScreen(),
    );
  }
}
