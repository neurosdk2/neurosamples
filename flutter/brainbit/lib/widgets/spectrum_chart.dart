import 'dart:async';
import 'dart:math';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:neurosdk2/neurosdk2.dart';
import 'package:spectrum_lib/spectrum_lib.dart';

final class ChanelSpectrum {
  final deltaPoints = <FlSpot>[];
  final thetaPoints = <FlSpot>[];
  final alphaPoints = <FlSpot>[];
  final betaPoints = <FlSpot>[];
  final otherPoints = <FlSpot>[];
}

class SpectrumChart extends StatefulWidget {
  final BrainBit sensor;
  const SpectrumChart({super.key, required this.sensor});

  @override
  State<SpectrumChart> createState() => _SpectrumChartState();
}

class _SpectrumChartState extends State<SpectrumChart> {
  static const int sampleRate = 250;

  static const Color alphaColor = Color.fromARGB(255, 78, 97, 109);
  static const Color betaColor = Color.fromARGB(255, 78, 109, 82);
  static const Color thetaColor = Color.fromARGB(255, 97, 78, 109);
  static const Color gammaColor = Color.fromARGB(255, 109, 108, 78);
  static const Color deltaColor = Color.fromARGB(255, 109, 88, 78);

  final List<double> samples = List.filled(sampleRate * 2, 0, growable: false);

  final ChanelSpectrum o1Data = ChanelSpectrum();
  final ChanelSpectrum o2Data = ChanelSpectrum();
  final ChanelSpectrum t3Data = ChanelSpectrum();
  final ChanelSpectrum t4Data = ChanelSpectrum();

  late final SpectrumLib o1SpectrumLib;
  late final SpectrumLib o2SpectrumLib;
  late final SpectrumLib t3SpectrumLib;
  late final SpectrumLib t4SpectrumLib;

  int currentChanel = 0;
  int currentWindow = 62;
  int currentAmplitude = 2;
  double currentFftBinsFor1Hz = 1.024;

  StreamSubscription? signalSubscription;

  @override
  void initState() {
    super.initState();

    o1SpectrumLib = _createSpectrumLib();
    o2SpectrumLib = _createSpectrumLib();
    t3SpectrumLib = _createSpectrumLib();
    t4SpectrumLib = _createSpectrumLib();

    signalSubscription = widget.sensor.signalDataStream.listen(onDataReceived);

    widget.sensor.execute(SensorCommand.startSignal);
  }

  SpectrumLib _createSpectrumLib() {
    final math = SpectrumLib(sampleRate, sampleRate * 4, 10);

    math.initParams(125, false);
    math.setWavesCoeffs(1, 1, 1, 1, 1);
    math.setSquaredSpectrum(false);

    return math;
  }

  @override
  void dispose() {
    super.dispose();
    signalSubscription?.cancel();
    widget.sensor.execute(SensorCommand.stopSignal);

    o1SpectrumLib.dispose();
    o2SpectrumLib.dispose();
    t3SpectrumLib.dispose();
    t4SpectrumLib.dispose();
  }

  LineChartBarData signalLine(List<FlSpot> points, Color color) {
    return LineChartBarData(
      spots: points,
      show: points.isNotEmpty,
      dotData: const FlDotData(show: false),
      color: color,
      barWidth: 2,
      isCurved: false,
      belowBarData: BarAreaData(
        show: true,
        color: color.withAlpha(150),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text(
          "Spectrum Graph",
          style: TextStyle(fontSize: 20, color: alphaColor),
        ),
        buildChartText("O1", alphaColor),
        buildChart(o1Data, alphaColor),
        buildChartText("O2", betaColor),
        buildChart(o2Data, betaColor),
        buildChartText("T3", gammaColor),
        buildChart(t3Data, gammaColor),
        buildChartText("T4", deltaColor),
        buildChart(t4Data, deltaColor),
      ],
    );
  }

  Widget buildValueCard(String name, double value, double percent, Color color) {
    return Column(
      children: [
        Text(
          name,
          style: TextStyle(fontSize: 15, color: color, fontWeight: FontWeight.bold),
        ),
        Text(
          value.toStringAsFixed(2),
          style: TextStyle(fontSize: 15, color: color),
        ),
        Text(
          "${percent.toStringAsFixed(2)}%",
          style: TextStyle(fontSize: 15, color: color),
        ),
      ],
    );
  }

  Widget buildChart(ChanelSpectrum spectrumData, Color color) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(bottom: 5.0),
        child: LineChart(
          LineChartData(
            maxY: currentAmplitude.toDouble(),
            minY: 0,
            minX: 0,
            maxX: currentWindow.toDouble(),
            lineTouchData: const LineTouchData(enabled: false),
            clipData: const FlClipData.all(),
            gridData: const FlGridData(
              show: true,
              drawVerticalLine: false,
              drawHorizontalLine: false,
            ),
            borderData: FlBorderData(
              show: true,
              border: Border.all(color: color),
            ),
            lineBarsData: [
              signalLine(spectrumData.deltaPoints, deltaColor),
              signalLine(spectrumData.thetaPoints, thetaColor),
              signalLine(spectrumData.alphaPoints, alphaColor),
              signalLine(spectrumData.betaPoints, betaColor),
              signalLine(spectrumData.otherPoints, gammaColor),
            ],
            titlesData: const FlTitlesData(
              show: false,
            ),
          ),
        ),
      ),
    );
  }

  Widget buildChartText(String name, Color color) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Text(name, style: TextStyle(fontSize: 15, color: color)),
    );
  }

  void onDataReceived(List<BrainBitSignalData> event) {
    if (event.isEmpty) return;

    final List<double> newSamples = [];

    newSamples.addAll(event.map((v) => v.o1));
    processSamples(o1SpectrumLib, newSamples, o1Data, "O1");
    newSamples.clear();

    newSamples.addAll(event.map((v) => v.o2));
    processSamples(o2SpectrumLib, newSamples, o2Data, "O2");
    newSamples.clear();

    newSamples.addAll(event.map((v) => v.t3));
    processSamples(t3SpectrumLib, newSamples, t3Data, "T3");
    newSamples.clear();

    newSamples.addAll(event.map((v) => v.t4));
    processSamples(t4SpectrumLib, newSamples, t4Data, "T4");
    newSamples.clear();

    setState(() {});
  }

  void processSamples(SpectrumLib lib, List<double> newSamples, ChanelSpectrum spectrumData, String chanelName) {
    lib.pushAndProcessData(newSamples);

    var wavesData = lib.readWavesSpectrumInfoArr();
    var rawSpectrumData = lib.readRawSpectrumInfoArr();

    lib.setNewSampleSize();

    processWavesData(wavesData, chanelName);
    processRawSpectrum(spectrumData, rawSpectrumData);
  }

  void processWavesData(List<WavesSpectrumData> wavesData, String chanelName) {
    if (wavesData.isEmpty) return;

    var lastData = wavesData[wavesData.length - 1];

    final alphaValue = lastData.alphaRaw;
    final betaValue = lastData.betaRaw;
    final thetaValue = lastData.thetaRaw;
    final gammaValue = lastData.gammaRaw;
    final deltaValue = lastData.deltaRaw;

    final alphaValuePercent = lastData.alphaRel * 100;
    final betaValuePercent = lastData.betaRel * 100;
    final thetaValuePercent = lastData.thetaRel * 100;
    final gammaValuePercent = lastData.gammaRel * 100;
    final deltaValuePercent = lastData.deltaRel * 100;

    print("""
$chanelName:
alphaValue: $alphaValue
betaValue: $betaValue
thetaValue: $thetaValue
gammaValue: $gammaValue
deltaValue: $deltaValue
alphaValuePercent: $deltaValuePercent
betaValuePercent: $gammaValuePercent
thetaValuePercent: $thetaValuePercent
gammaValuePercent: $betaValuePercent
deltaValuePercent: $alphaValuePercent
""");
  }

  void processRawSpectrum(ChanelSpectrum chanelData, List<RawSpectrumData> data) {
    if (data.isEmpty) return;

    for (var sample in data.where((s) => s.allBinsValues.isNotEmpty)) {
      if (sample.allBinsValues.isEmpty) continue;

      var length = min(samples.length, sample.allBinsValues.length);
      for (var i = 0; i < length; i++) {
        samples[i] = sample.allBinsValues[i] * 1e3;
      }
    }

    chanelData.deltaPoints.clear();
    chanelData.thetaPoints.clear();
    chanelData.betaPoints.clear();
    chanelData.alphaPoints.clear();
    chanelData.otherPoints.clear();

    int lastIndex = min((currentWindow * currentFftBinsFor1Hz).ceil(), samples.length);
    double xStep = currentWindow / lastIndex;

    var points = <FlSpot>[];

    for (var i = 0; i < lastIndex; i++) {
      var x = xStep * i;
      var y = samples[i];

      switch (i) {
        case 4:
          addPointsAndClear(FlSpot(x, y), chanelData.deltaPoints, points);
          break;
        case 8:
          addPointsAndClear(FlSpot(x, y), chanelData.thetaPoints, points);
          break;
        case 14:
          addPointsAndClear(FlSpot(x, y), chanelData.alphaPoints, points);
          break;
        case 34:
          addPointsAndClear(FlSpot(x, y), chanelData.betaPoints, points);
          break;
      }

      points.add(FlSpot(x, y));
    }

    if (points.isNotEmpty) {
      chanelData.otherPoints.addAll(points);
    }
  }

  void addPointsAndClear(FlSpot newPoint, List<FlSpot> to, List<FlSpot> from) {
    from.add(newPoint);
    to.addAll(from);
    from.clear();
  }
}
