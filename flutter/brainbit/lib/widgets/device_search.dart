import 'dart:async';

import 'package:flutter/material.dart';
import 'package:neurosdk2/neurosdk2.dart';
import 'package:neurosdk2_brainbit_demo/widgets/found_device_list_element.dart';

class DeviceSearch extends StatefulWidget {
  final Scanner scanner;
  final void Function(SensorInfo) deviceSelected;
  const DeviceSearch(
      {super.key, required this.scanner, required this.deviceSelected});

  @override
  State<DeviceSearch> createState() => DeviceSearchState();
}

class DeviceSearchState extends State<DeviceSearch> {
  final List<SensorInfo> _foundSensors = [];

  StreamSubscription? _scannerSearchSubscription;

  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    _asyncInit();
  }

  @override
  void dispose() {
    widget.scanner.stop();
    _scannerSearchSubscription?.cancel();
    super.dispose();
  }

  Future _asyncInit() async {
    try {
      _scannerSearchSubscription = widget.scanner.sensorsStream.listen((event) {
        if (!mounted) return;

        setState(() {
          _isLoading = event.isEmpty;

          _foundSensors.clear();
          _foundSensors.addAll(event);
        });
      });

      widget.scanner.start();
    } on Exception {
      if (mounted) Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_isLoading) {
      return const Center(
        child: CircularProgressIndicator(),
      );
    }

    return Padding(
      padding: const EdgeInsets.only(top: 10, left: 5, right: 5),
      child: ListView(
        children: [
          for (final info in _foundSensors)
            FoundDeviceListElement(
              key: ValueKey(info.address),
              sensorInfo: info,
              scanner: widget.scanner,
              onTap: (info) {
                Navigator.pop(context);
                widget.deviceSelected.call(info);
              },
            ),
        ],
      ),
    );
  }
}
