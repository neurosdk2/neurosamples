import 'package:flutter/material.dart';

class BrainbitElectrode extends StatelessWidget {
  final String name;
  final Color color;
  const BrainbitElectrode({super.key, required this.name, required this.color});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 50,
      height: 30,
      child: Card(
        color: color,
        child: Text(name, textAlign: TextAlign.center),
      ),
    );
  }
}
