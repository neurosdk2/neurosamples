import 'dart:async';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:neurosdk2/neurosdk2.dart';
import 'package:neurosdk2_brainbit_demo/utils/dynamic_chart_data.dart';

class SignalChart extends StatefulWidget {
  final BrainBit sensor;
  const SignalChart({super.key, required this.sensor});

  @override
  State<SignalChart> createState() => _SignalChartState();
}

class _SignalChartState extends State<SignalChart> {
  static const double windowLength = 2000;
  static const List<double> amplitudeList = [1, 1.5, 2];

  static const Color o1Color = Color.fromARGB(255, 78, 97, 109);
  static const Color o2Color = Color.fromARGB(255, 78, 109, 82);
  static const Color t3Color = Color.fromARGB(255, 109, 108, 78);
  static const Color t4Color = Color.fromARGB(255, 109, 88, 78);

  final DynamicChartData o1ChartData = DynamicChartData(windowLength);
  final DynamicChartData o2ChartData = DynamicChartData(windowLength);
  final DynamicChartData t3ChartData = DynamicChartData(windowLength);
  final DynamicChartData t4ChartData = DynamicChartData(windowLength);

  double currentAmplitude = amplitudeList[0];

  StreamSubscription? signalSubscription;

  @override
  void initState() {
    super.initState();

    signalSubscription = widget.sensor.signalDataStream.listen(
      (event) {
        List<double> samples = [];

        samples.addAll(event.map((v) => v.o1));
        o1ChartData.add(samples);

        samples.clear();
        samples.addAll(event.map((v) => v.o2));
        o2ChartData.add(samples);

        samples.clear();
        samples.addAll(event.map((v) => v.t3));
        t3ChartData.add(samples);

        samples.clear();
        samples.addAll(event.map((v) => v.t4));
        t4ChartData.add(samples);

        setState(() {});
      },
    );

    widget.sensor.execute(SensorCommand.startSignal);

    o1ChartData.start();
    o2ChartData.start();
    t3ChartData.start();
    t4ChartData.start();
  }

  @override
  void dispose() {
    super.dispose();
    signalSubscription?.cancel();
    widget.sensor.execute(SensorCommand.stopSignal);
  }

  LineChartBarData signalLine(List<FlSpot> points, Color color) {
    return LineChartBarData(
      spots: points,
      show: points.isNotEmpty,
      dotData: const FlDotData(show: false),
      color: color,
      barWidth: 2,
      isCurved: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Signal Graph",
          style: TextStyle(
            fontSize: 20,
            color: Theme.of(context).colorScheme.primary,
          ),
        ),
        Row(
          children: [
            Text(
              "Amplitude (µV):",
              style: TextStyle(
                fontSize: 15,
                color: Theme.of(context).colorScheme.secondary,
              ),
            ),
            const Spacer(),
            SegmentedButton<double>(
              showSelectedIcon: false,
              style: SegmentedButton.styleFrom(
                backgroundColor: Colors.transparent,
                foregroundColor: Theme.of(context).colorScheme.secondary,
                selectedForegroundColor: Colors.white,
                selectedBackgroundColor: Theme.of(context).colorScheme.primary,
                visualDensity:
                    const VisualDensity(horizontal: -3, vertical: -3),
              ),
              segments: <ButtonSegment<double>>[
                for (final value in amplitudeList)
                  ButtonSegment<double>(
                    value: value,
                    label: Text(value.toString()),
                  ),
              ],
              selected: <double>{currentAmplitude},
              onSelectionChanged: (amplitude) =>
                  setState(() => currentAmplitude = amplitude.first),
            ),
          ],
        ),
        buildChartText("O1", o1ChartData, o1Color),
        buildChart(o1ChartData, o1Color),
        buildChartText("O2", o2ChartData, o2Color),
        buildChart(o2ChartData, o2Color),
        buildChartText("T3", t3ChartData, t3Color),
        buildChart(t3ChartData, t3Color),
        buildChartText("T4", t4ChartData, t4Color),
        buildChart(t4ChartData, t4Color),
      ],
    );
  }

  // 4e616d
  Widget buildChartText(String name, DynamicChartData data, Color color) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Text(
        '$name: ${data.points.isNotEmpty ? data.points.last.y : "-"}',
        style: TextStyle(
          fontSize: 15,
          color: color,
        ),
      ),
    );
  }

  Widget buildChart(DynamicChartData data, Color color) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(bottom: 24.0),
        child: LineChart(
          LineChartData(
            maxY: currentAmplitude,
            minY: -currentAmplitude,
            minX: 0,
            maxX: windowLength,
            lineTouchData: const LineTouchData(enabled: false),
            clipData: const FlClipData.all(),
            gridData: const FlGridData(
              show: true,
              drawVerticalLine: false,
              drawHorizontalLine: false,
            ),
            borderData: FlBorderData(
              show: true,
              border: Border.all(color: color),
            ),
            lineBarsData: [
              signalLine(data.points, color),
            ],
            titlesData: const FlTitlesData(
              show: false,
            ),
          ),
        ),
      ),
    );
  }
}
