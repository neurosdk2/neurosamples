//
//  ViewController.swift
//  TestSDKSwift
//
//  Created by Aseatari on 29.11.2022.
//

import UIKit
import neurosdk2

class MainViewController: UIViewController {

    @IBOutlet weak var SearchButton: UIButton!
    @IBOutlet weak var DeviceInfoButton: UIButton!
    @IBOutlet weak var SignalButton: UIButton!
    @IBOutlet weak var ResistanceButton: UIButton!
    @IBOutlet weak var SpectrumButton: UIButton!
    @IBOutlet weak var EmotionsButton: UIButton!
    
    @IBOutlet weak var MonopolarEmotions: UIButton!
    @IBOutlet weak var PowerLabel: UIBarButtonItem!
    @IBOutlet weak var StateLabel: UIBarButtonItem!
        
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        let controller = BrainbitController.shared
        
        controller.connectionStateChanged = { (_ state : NTSensorState) -> Void in
            self.onStateChange(state: state)
        }
        
        controller.batteryChanged = {(_ power : NSNumber)  -> Void in
            self.onPowerChange(power: power)
        }
        
        self.onStateChange(state: controller.connectionState ?? NTSensorState.outOfRange)
    }
    
    func onPowerChange(power: NSNumber) {
        DispatchQueue.main.async {
            self.PowerLabel.title = power.stringValue + "%"
        }
    }
    
    func onStateChange(state: NTSensorState) {
        DispatchQueue.main.async {
            switch state {
            case .inRange:
                self.DeviceInfoButton.isEnabled = true
                self.SignalButton.isEnabled = true
                self.ResistanceButton.isEnabled = true
                self.EmotionsButton.isEnabled = true
                self.SpectrumButton.isEnabled = true
                self.MonopolarEmotions.isEnabled = true
                self.StateLabel.title = "Connected"
                break
            case .outOfRange:
                self.DeviceInfoButton.isEnabled = false
                self.SignalButton.isEnabled = false
                self.ResistanceButton.isEnabled = false
                self.EmotionsButton.isEnabled = false
                self.SpectrumButton.isEnabled = false
                self.MonopolarEmotions.isEnabled = false
                self.StateLabel.title = "Disconnected"
                self.PowerLabel.title = "0"
                break
            default:
                break
            }
        }
        
    }

}
