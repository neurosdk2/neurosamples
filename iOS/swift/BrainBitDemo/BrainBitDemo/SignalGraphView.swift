//
//  SignalGraphView.swift
//  TestSDKSwift
//
//  Created by Aseatari on 29.11.2022.
//

import Foundation
import CorePlot

class SignalGraphView: CPTGraphHostingView, CPTPlotDataSource, CPTPlotSpaceDelegate {
    
    var plotData : NSMutableArray = [0.0]
    var plot: CPTScatterPlot!
    var maxDataPoints = 250 * 3
    var amplitude = 0.001
    
    func initGraph(samplingFrequency: Int, channelName: String) {
        self.allowPinchScaling = false
        self.plotData.removeAllObjects()
        for idx in 0...maxDataPoints {
            plotData.add(0.0)
        }
                
        maxDataPoints = samplingFrequency * 3
        
        //Configure graph
        let graph = CPTXYGraph(frame: self.bounds)
        graph.plotAreaFrame?.masksToBorder = false
        self.hostedGraph = graph
        graph.backgroundColor = UIColor.white.cgColor
        graph.paddingBottom = 10
        graph.paddingLeft = 40.0
        graph.paddingTop = 10
        graph.paddingRight = 10.0
        
        //Style for graph title
        let titleStyle = CPTMutableTextStyle()
        titleStyle.color = CPTColor.black()
        titleStyle.fontName = "HelveticaNeue-Bold"
        titleStyle.fontSize = 20.0
        titleStyle.textAlignment = .center
        graph.titleTextStyle = titleStyle

        //Set graph title
        graph.title = channelName
        graph.titlePlotAreaFrameAnchor = .top
        graph.titleDisplacement = CGPoint(x: 0.0, y: 0.0)
        
        let axisSet = graph.axisSet as! CPTXYAxisSet
        
        let axisTextStyle = CPTMutableTextStyle()
        axisTextStyle.color = CPTColor.black()
        axisTextStyle.fontName = "HelveticaNeue-Bold"
        axisTextStyle.fontSize = 10.0
        axisTextStyle.textAlignment = .center
        let lineStyle = CPTMutableLineStyle()
        lineStyle.lineColor = CPTColor.black()
        lineStyle.lineWidth = 2
        let gridLineStyle = CPTMutableLineStyle()
        gridLineStyle.lineColor = CPTColor.gray()
        gridLineStyle.lineWidth = 0.5
       

        if let x = axisSet.xAxis {
            x.majorIntervalLength   = (samplingFrequency) as NSNumber
            x.minorTicksPerInterval = UInt(samplingFrequency)
            x.labelTextStyle = axisTextStyle
            x.minorGridLineStyle = gridLineStyle
            x.axisLineStyle = lineStyle
            x.axisConstraints = CPTConstraints(lowerOffset: 0.0)
            x.delegate = self
        }

        if let y = axisSet.yAxis {
            y.majorIntervalLength   = 25
            y.minorTicksPerInterval = UInt(samplingFrequency)
            y.minorGridLineStyle = gridLineStyle
            y.labelTextStyle = axisTextStyle
            y.axisLineStyle = lineStyle
            y.axisConstraints = CPTConstraints(lowerOffset: 5.0)
            y.delegate = self
        }

        // Set plot space
        let xMin = 0.0
        let xMax = maxDataPoints
        let yMin = -amplitude
        let yMax =  amplitude
        guard let plotSpace = graph.defaultPlotSpace as? CPTXYPlotSpace else { return }
        plotSpace.xRange = CPTPlotRange(locationDecimal: CPTDecimalFromDouble(xMin), lengthDecimal: CPTDecimalFromDouble(Double(xMax)))
        plotSpace.yRange = CPTPlotRange(locationDecimal: CPTDecimalFromDouble(yMin), lengthDecimal: CPTDecimalFromDouble(yMax - yMin))
        
        let plotLineStile = CPTMutableLineStyle()
        plotLineStile.lineJoin = .round
        plotLineStile.lineCap = .round
        plotLineStile.lineWidth = 1
        plotLineStile.lineColor = CPTColor.red()
        
        plot = CPTScatterPlot()
        plot.dataLineStyle = plotLineStile
        plot.identifier = "signal-graph" as NSCoding & NSCopying & NSObjectProtocol
        guard let graph1 = self.hostedGraph else { return }
        plot.dataSource = (self as CPTPlotDataSource)
        plot.delegate = (self as CALayerDelegate)
        graph.add(plot, to: graph1.defaultPlotSpace)
    }
    
    func dataChanged(newValues: [NSNumber]){
        let graph = plot.graph
        
        if(newValues.count > 0){
            for i in 0...newValues.count - 1{
                plotData.removeObject(at: 0)
                plotData.add(newValues[i])
            }
        }
        
        plot.reloadData()
    }
    
    
    func setAmplitude(amp: Double) {
        amplitude = amp
    }
    
    
    func numberOfRecords(for plot: CPTPlot) -> UInt {
        return UInt(maxDataPoints)
    }
    
    func numbers(for plot: CPTPlot, field fieldEnum: UInt, recordIndexRange indexRange: NSRange) -> [Any]? {
        
//        let plotSpace = plot.plotSpace! as CPTPlotSpace
//        let graph = plotSpace.graph
        
        guard let plotSpace = plot.plotSpace as? CPTXYPlotSpace else { return [] }
        plotSpace.yRange = CPTPlotRange(locationDecimal: CPTDecimalFromDouble(-amplitude), lengthDecimal: CPTDecimalFromDouble(amplitude + amplitude))
        
        if(fieldEnum == CPTScatterPlotField.X.rawValue){
            var array:[NSNumber] = []
            for i in indexRange.location..<indexRange.location + indexRange.length {
                array.append(NSNumber.init(value: i))
            }
            return array;
        }else if(fieldEnum == CPTScatterPlotField.Y.rawValue){
            let indexes: NSIndexSet? = NSIndexSet(indexesIn: indexRange)
            return indexes?.map { plotData[$0] }
        }
        
        return []
    }
    
}
