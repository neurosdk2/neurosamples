package com.neurotech.brainbitneurosdkdemo.spectrum

enum class SignalChannels {
    O1,
    O2,
    T3,
    T4
}