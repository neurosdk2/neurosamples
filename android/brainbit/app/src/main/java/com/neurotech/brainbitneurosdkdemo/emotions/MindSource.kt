package com.neurotech.brainbitneurosdkdemo.emotions

enum class MindSource {
    Relax,
    Concentration,
    InstRelax,
    InstConcentration
}