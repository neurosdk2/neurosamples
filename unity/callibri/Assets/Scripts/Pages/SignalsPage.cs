using UnityEngine;
using TMPro;
using System.Collections;
using System.Collections.Generic;
using NeuroSDK;
using Neurotech.Filters;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine.UI;
using System;
using static UnityEditor.Progress;

public class SignalsPage : MonoBehaviour, IPage
{
    [SerializeField] private TMP_Dropdown _dropdownFiltersLP;
    [SerializeField] private TMP_Dropdown _dropdownFiltersHP;
    [SerializeField] private TMP_Dropdown _dropdownFiltersBS;
    [SerializeField] private TextMeshProUGUI _startSignalText;
    [SerializeField] private ChartManager _signalChart;
    [SerializeField] private TextMeshProUGUI _electrodeStateText;
    private Filter _filters = new Filter();
    private Coroutine _updateSignalCoroutine;
    private List<CallibriSignalData> _signalData = new List<CallibriSignalData>();
    private readonly object locker = new object();

    private bool _started = false;
    private bool started
    {
        get => _started;
        set
        {
            if (value != _started)
            {
                _started = value;
                _startSignalText.text = _started ? "Stop" : "Start";
            }
        }
    }

    private IEnumerator UpdateSignalCharts()
    {
        while (true)
        {
            lock (locker)
            {
                int totalSamplesLength = 0;
                foreach (var sample in _signalData)
                {
                    totalSamplesLength += sample.Samples.Length;
                }

                if (totalSamplesLength < 0) continue;

                var signalSamples = new double[totalSamplesLength];

                int signalSamplesIndex = 0;
                foreach (var sample in _signalData)
                {
                    for (int i = 0; i < sample.Samples.Length; i++)
                    {
                        signalSamples[signalSamplesIndex] = sample.Samples[i];
                        signalSamplesIndex++;
                    }
                }
                _filters.filterList.FilterArray(signalSamples);
                _signalChart.AddData(signalSamples);
                _signalData.Clear();
            }
            yield return new WaitForSeconds(0.06f);
        }
    }

    public void UpdateSignal()
    {
        if (started)
        {
            CallibriController.Instance.StopSignal();
            CallibriController.Instance.signalReceived -= OnSignalReceived;

            CallibriController.Instance.StopElectrode();
            CallibriController.Instance.electrodeStateChanged -= OnElectrodeStateChanged;

            started = false;
            return;
        }

        CallibriController.Instance.electrodeStateChanged += OnElectrodeStateChanged;
        CallibriController.Instance.StartElectrode();

        CallibriController.Instance.signalReceived += OnSignalReceived;
        CallibriController.Instance.StartSignal();
        started = true;
    }

    private void OnSignalReceived(CallibriSignalData[] samples)
    {
        if (samples is null && samples.Length < 0) return;
        lock (locker)
        {
            _signalData.AddRange(samples);
        }
    }

    private void OnElectrodeStateChanged(CallibriElectrodeState state)
    {
        MainThreadDispatcher.RunOnMainThread(() => _electrodeStateText.text = state.ToString());
    }

    public void PickItemDropdown()
    {
        _filters.PickItemFilters(_dropdownFiltersLP, _dropdownFiltersHP, _dropdownFiltersBS);
    }

    private void AddFilter()
    {
        _dropdownFiltersLP.options.Clear();
        _dropdownFiltersHP.options.Clear();
        _dropdownFiltersBS.options.Clear();
        _dropdownFiltersLP.options.Add(new TMP_Dropdown.OptionData($"���"));
        _dropdownFiltersHP.options.Add(new TMP_Dropdown.OptionData($"���"));
        _dropdownFiltersBS.options.Add(new TMP_Dropdown.OptionData($"���"));
        _filters.FiltersAddLists(_dropdownFiltersLP, _dropdownFiltersHP, _dropdownFiltersBS);
    }


    private void OnEnable()
    {
        Enter();
    }

    private void OnDisable()
    {
        Exit();
    }

    public void Enter()
    {
        _updateSignalCoroutine = StartCoroutine(UpdateSignalCharts());
        _electrodeStateText.text = CallibriElectrodeState.ElStDetached.ToString();
        AddFilter();
    }

    public void Exit()
    {
        if (started)
        {
            started = !started;
        }
        StopCoroutine(_updateSignalCoroutine);
        CallibriController.Instance.StopSignal();
        CallibriController.Instance.signalReceived -= OnSignalReceived;
        CallibriController.Instance.StopElectrode();
        CallibriController.Instance.electrodeStateChanged -= OnElectrodeStateChanged;
        _signalChart.PrepareClearData();
        _signalData.Clear();
        _dropdownFiltersLP.value = 0;
        _dropdownFiltersHP.value = 0;
        _dropdownFiltersBS.value = 0;
        _filters.PickItemFilters(_dropdownFiltersLP, _dropdownFiltersHP, _dropdownFiltersBS);
    }
}
