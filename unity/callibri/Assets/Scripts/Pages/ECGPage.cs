using NeuroSDK;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class ECGPage : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _buttonText;
    [SerializeField] private TextMeshProUGUI _RRText;
    [SerializeField] private TextMeshProUGUI _HRText;
    [SerializeField] private TextMeshProUGUI _pressureIndexText;
    [SerializeField] private TextMeshProUGUI _modaText;
    [SerializeField] private TextMeshProUGUI _amplModaText;
    [SerializeField] private TextMeshProUGUI _RRDetected;
    private float _RR = 0;
    private float _HR = 0;
    private float _pressureIndex = 0;
    private float _moda = 0;
    private float _amplModa = 0;
    private ECGController _controller;
    private IEnumerator _updateECGCoroutine;
    int _samplingFrequencyHz = 1000;

    private bool _started = false;
    private bool started
    {
        get { return _started; }
        set
        {
            if (value != _started)
            {
                _started = value;
                _buttonText.text = _started ? "Stop" : "Start";
            }

        }
    }

    private void OnProcessedResult(ECGData data)
    {
        string EcgStatus = $"{nameof(data.RR)}: {data.RR:N}\n"
                  + $"{nameof(data.HR)}: {data.HR:N}\n"
                  + $"{nameof(data.PressureIndex)}: {data.PressureIndex:N}\n"
                  + $"{nameof(data.Moda)}: {data.Moda:N}\n"
                  + $"{nameof(data.AmplModa)}: {data.AmplModa:N}\n"
                  + $"{nameof(data.VariationDist)}: {data.VariationDist:N}";
         _RR = MathF.Round((float)data.RR, 2);
         _HR = MathF.Round((float)data.HR, 2);
         _pressureIndex = MathF.Round((float)data.PressureIndex, 2);
         _moda = MathF.Round((float)data.Moda, 2);
         _amplModa = MathF.Round((float)data.AmplModa, 2);
}


    private IEnumerator UpdateValues()
    {
        while (true)
        {
            _RRDetected.text = _controller.rrDetected.ToString();
            _RRText.text = _RR.ToString();
            _HRText.text = _HR.ToString();
            _pressureIndexText.text = _pressureIndex.ToString();
            _modaText.text = _moda.ToString();
            _amplModaText.text = _amplModa.ToString();
            yield return new WaitForSeconds(0.16f);
        }
    }

    public void UpdateECG()
    {
        if (started)
        {
            CallibriController.Instance.StopSignal();
            CallibriController.Instance.signalReceived -= OnSignalReceived;
            _controller.ProcessedResult = null;
        }
        else
        {
            CallibriController.Instance.StartSignal();
            CallibriController.Instance.signalReceived += OnSignalReceived;
            _controller.ProcessedResult = OnProcessedResult;
        }
        started = !started;
    }

    private void OnSignalReceived(CallibriSignalData[] samples)
    {
        if (samples is null && samples.Length < 0) return;
        _controller.ProcessSamples(samples.ToList().SelectMany(s => s.Samples).ToArray());
    }

    private void OnEnable()
    {
        Enter();
    }

    private void OnDisable()
    {
        Exit();
    }

    public void Enter()
    {
        _controller = new ECGController(_samplingFrequencyHz);
        _controller.InitFilter();
        _updateECGCoroutine = UpdateValues();
        StartCoroutine(_updateECGCoroutine);
    }

    public void Exit()
    {
        if (started)
        {
            started = !started;
        }
        StopCoroutine(_updateECGCoroutine);
        CallibriController.Instance.signalReceived -= OnSignalReceived;
        CallibriController.Instance.StopSignal();
        _RR = 0;
        _HR = 0;
        _pressureIndex = 0;
        _moda = 0;
        _amplModa = 0;
        _controller.Dispose();
        _controller = null;
    }
}
