using NeuroSDK;
using NeuroTech.Spectrum;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class SpectrumPage : MonoBehaviour
{
    [SerializeField] private ChartManagerSpectrum _chart;
    [SerializeField] private TextMeshProUGUI _buttonText;
    [SerializeField] private TextMeshProUGUI _deltaRawText;
    [SerializeField] private TextMeshProUGUI _thetaRawText;
    [SerializeField] private TextMeshProUGUI _alphaRawText;
    [SerializeField] private TextMeshProUGUI _betaRawText;
    [SerializeField] private TextMeshProUGUI _gammaRawText;
    [SerializeField] private TextMeshProUGUI _deltaRelText;
    [SerializeField] private TextMeshProUGUI _thetaRelText;
    [SerializeField] private TextMeshProUGUI _alphaRelText;
    [SerializeField] private TextMeshProUGUI _betaRelText;
    [SerializeField] private TextMeshProUGUI _gammaRelText;
    double avgAlphaRaw = 0;
    double avgBetaRaw = 0;
    double avgGammaRaw = 0;
    double avgDeltaRaw = 0;
    double avgThetaRaw = 0;
    double avgAlphaRel = 0;
    double avgBetaRel = 0;
    double avgGammaRel = 0;
    double avgDeltaRel = 0;
    double avgThetaRel = 0;
    private SpectrumController _controller;
    private double[] _spectrumData;
    private List<CallibriSignalData> _signalData = new List<CallibriSignalData>();
    int _samplingFrequencyHz = 1000;
    private IEnumerator _updateSpectrumCoroutine;
    private readonly object locker = new object();

    private enum ChannelType
    {
        O1,
        O2,
        T3,
        T4
    }
    private bool _started = false;
    private bool started
    {
        get { return _started; }
        set
        {
            if (value != _started)
            {
                _started = value;
                _buttonText.text = _started ? "Stop" : "Start";
            }

        }
    }

    private void SpectrumPageStart()
    {
        _controller = new SpectrumController(_samplingFrequencyHz);
        
    }

    private void OnProcessedData(IReadOnlyCollection<RawSpectrumData> rawSpectrumData, IReadOnlyCollection<WavesSpectrumData> wavesSpectrumData)
    {
        if (wavesSpectrumData.Count > 0)
        {
            avgDeltaRaw = wavesSpectrumData.Select(it => it.DeltaRaw).Average();
            avgThetaRaw = wavesSpectrumData.Select(it => it.ThetaRaw).Average();
            avgAlphaRaw = wavesSpectrumData.Select(it => it.Alpha_Raw).Average();
            avgBetaRaw = wavesSpectrumData.Select(it => it.BetaRaw).Average();
            avgGammaRaw = wavesSpectrumData.Select(it => it.GammaRaw).Average();
            avgDeltaRel = wavesSpectrumData.Select(it => it.DeltaRel).Average();
            avgThetaRel = wavesSpectrumData.Select(it => it.ThetaRel).Average();
            avgAlphaRel = wavesSpectrumData.Select(it => it.Alpha_Rel).Average();
            avgBetaRel = wavesSpectrumData.Select(it => it.BetaRel).Average();
            avgGammaRel = wavesSpectrumData.Select(it => it.GammaRel).Average();

            if (rawSpectrumData.Count <= 0) { return; }
            else
            {
                foreach (RawSpectrumData spectrumData in rawSpectrumData.Where(it => it.AllBinsValues.Length != 0)) _spectrumData = spectrumData.AllBinsValues;
            }
        }
    }

    private IEnumerator UpdateValues()
    {
        while (true)
        {
            lock (locker)
            {
                if (_spectrumData != null) _chart.AddData(_spectrumData);
                
                _deltaRawText.text = MathF.Round((float)avgDeltaRaw, 2).ToString();
                _thetaRawText.text = MathF.Round((float)avgThetaRaw, 2).ToString();
                _alphaRawText.text = MathF.Round((float)avgAlphaRaw, 2).ToString();
                _betaRawText.text = MathF.Round((float)avgBetaRaw, 2).ToString();
                _gammaRawText.text = MathF.Round((float)avgGammaRaw, 2).ToString();
                _deltaRelText.text = $"{(int)(avgDeltaRel * 100)}%";
                _thetaRelText.text = $"{(int)(avgThetaRel * 100)}%";
                _alphaRelText.text = $"{(int)(avgAlphaRel * 100)}%";
                _betaRelText.text = $"{(int)(avgBetaRel * 100)}%";
                _gammaRelText.text = $"{(int)(avgGammaRel * 100)}%";
            }
            yield return new WaitForSeconds(0.16f);
        }
    }

    public void UpdateSpectrum()
    {
        if (started)
        {
            CallibriController.Instance.StopSignal();
            CallibriController.Instance.signalReceived -= OnSignalReceived;
            _controller =null;
        }
        else
        {
            CallibriController.Instance.StartSignal();
            CallibriController.Instance.signalReceived += OnSignalReceived;
            _controller.ProcessedData = OnProcessedData;
        }
        started = !started;
    }

    private void OnSignalReceived(CallibriSignalData[] samples)
    {
        if (samples is null && samples.Length < 0) return;
        lock (locker)
        {
            _signalData.AddRange(samples);
            _controller.ProcessSamples(_signalData.SelectMany(it => it.Samples).ToArray());
        }
    }
    
    private void OnEnable()
    {
        SpectrumPageStart();
        Enter();
    }

    private void OnDisable()
    {
        Exit();
    }

    public void Enter()
    {
        _updateSpectrumCoroutine = UpdateValues();
        StartCoroutine(_updateSpectrumCoroutine);
    }

    public void Exit()
    {
        if (started)
        {
            started = !started;
        }
        StopCoroutine(_updateSpectrumCoroutine);
        CallibriController.Instance.signalReceived -= OnSignalReceived;
        CallibriController.Instance.StopSignal();
        _controller = null;
        _chart.PrepareClearData();
        _spectrumData = null;
        avgAlphaRaw = 0;
        avgBetaRaw = 0;
        avgGammaRaw = 0;
        avgDeltaRaw = 0;
        avgThetaRaw = 0;
        avgAlphaRel = 0;
        avgBetaRel = 0;
        avgGammaRel = 0;
        avgDeltaRel = 0;
        avgThetaRel = 0;
    }

}
