using System;
using System.Collections.Generic;
using NeuroSDK;
using UnityEngine;
using XCharts.Runtime;

public class ChartManagerSpectrum : MonoBehaviour
{
    [SerializeField] private LineChart _chart;
    [SerializeField] private string _seriesName;

    private int _maxCacheWindow = 1000;
    private Color _color = new Color(0.08561765f, 0.490566f, 0.4736722f);

    void Start()
    {
        PrepareClearData();
    }
    
    public void AddData(double[] samples)
    {
        if(_maxCacheWindow != samples.Length) _chart.SetMaxCache(samples.Length);
        for (int i = 0; i < samples.Length; i++)
        {
            _chart.AddData(0, i, samples[i]);
        }
        _chart.RefreshChart();
        _chart.series[0].lineStyle.color = _color;
        _chart.series[0].itemStyle.color = _color;
    }

    public void PrepareClearData()
    {
        _chart.RemoveData();
        _chart.AddSerie<Line>(_seriesName);
        _chart.ClearData();
        _chart.SetMaxCache(_maxCacheWindow);
    }

}
