using Neurotech.Filters;
using System.Collections.Generic;
using System.Linq;
using TMPro;

public class Filter
{
    private List<IIRFilterParam> _LPFilters = new List<IIRFilterParam>();
    private List<IIRFilterParam> _HPFilters = new List<IIRFilterParam>();
    private List<IIRFilterParam> _BSFilters = new List<IIRFilterParam>();
    public FilterList filterList = new FilterList();

    public Filter()
    {
        IIRFilterParam[] filters = PreinstalledFilters.List().Where(val => val.samplingFreq == 1000).ToArray();
        _LPFilters = filters.Where(val => val.type == IIRFilterType.FtLP).ToList();
        _HPFilters = filters.Where(val => val.type == IIRFilterType.FtHP).ToList();
        _BSFilters = filters.Where(val => val.type == IIRFilterType.FtBandStop).ToList();
    }

    public void FiltersAddLists(TMP_Dropdown _LPFiltersPicker, TMP_Dropdown _HPFiltersPicker, TMP_Dropdown _BSFiltersPicker)
    {
        foreach (var filter in _LPFilters) _LPFiltersPicker.options.Add(new TMP_Dropdown.OptionData($"{filter.cutoffFreq}"));
        foreach (var filter in _HPFilters) _HPFiltersPicker.options.Add(new TMP_Dropdown.OptionData($"{filter.cutoffFreq}"));
        foreach (var filter in _BSFilters) _BSFiltersPicker.options.Add(new TMP_Dropdown.OptionData($"{filter.cutoffFreq}"));
        PickItemFilters(_LPFiltersPicker, _HPFiltersPicker, _BSFiltersPicker);
    }

    public void PickItemFilters(TMP_Dropdown _LPFiltersPicker, TMP_Dropdown _HPFiltersPicker, TMP_Dropdown _BSFiltersPicker)
    {
        filterList.ClearFilters();
        if (_LPFiltersPicker.value != 0 && _HPFiltersPicker.value != 0 && _BSFiltersPicker.value != 0)
        {
            IIRFilter filterLP = IIRFilterBuilder.CreateWithParams(_LPFilters[_LPFiltersPicker.value-1]);
            IIRFilter filterHP = IIRFilterBuilder.CreateWithParams(_HPFilters[_HPFiltersPicker.value-1]);
            IIRFilter filterBS = IIRFilterBuilder.CreateWithParams(_BSFilters[_BSFiltersPicker.value-1]);
            filterList.AddFilter(filterLP);
            filterList.AddFilter(filterHP);
            filterList.AddFilter(filterBS);
        }
    }
}
