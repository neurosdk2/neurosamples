using Neurotech.CallibriUtils;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public struct ECGData
{
    public double RR;
    public double HR;
    public double PressureIndex;
    public double Moda;
    public double AmplModa;
    public double VariationDist;
}

public class ECGController 
{
    private readonly CallibriMath _math;
    public Action<ECGData> ProcessedResult;
    private readonly ConcurrentQueue<double> _samplesBuf = new();
    private readonly int _packSize;
    private readonly object locker = new object();
    public bool rrDetected;

    public ECGController(int samplingRate)
    {
        int dataWindow = samplingRate / 2;
        var nwinsForPressureIndex = 30;

        _math = new CallibriMath(samplingRate, dataWindow, nwinsForPressureIndex);
        _packSize = samplingRate / 10;
    }

    public void InitFilter() { _math.InitFilter(); }

    public void Dispose() { _math.Dispose(); }

    public void ProcessSamples(IEnumerable<double> samples)
    {
            try
            {
                foreach (var item in samples)
                {
                    _samplesBuf.Enqueue(item);
                }

                var pack = new double[_packSize];

                if (_samplesBuf.Count >= _packSize)
                    for (var i = 0; i < _packSize; i++)
                        _samplesBuf.TryDequeue(out pack[i]);
                else return;

                _math.PushData(pack);
                _math.ProcessDataArr();

                rrDetected = _math.RRdetected();
                if (rrDetected)
                {
                    ProcessedResult?.Invoke(
                    new ECGData
                    {
                        RR = _math.GetRR(),
                        HR = _math.GetHR(),
                        AmplModa = _math.GetAmplModa(),
                        Moda = _math.GetModa(),
                        PressureIndex = _math.GetPressureIndex(),
                        VariationDist = _math.GetVariationDist()
                    }
                );
                }
                _math.SetRRchecked();

            }
            catch (Exception ex)
            {
                Debug.LogError(ex.ToString());
            }
        }
}
