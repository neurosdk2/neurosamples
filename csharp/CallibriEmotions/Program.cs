﻿using CallibriEmotions;
using NeuroSDK;
using System.Collections.Concurrent;

/*** Create Emotions lib ***/
EmotionsController emotionController = new();

/*** Search sensor ***/
Console.WriteLine("Search devices (15s):");
Scanner scanner = new Scanner(SensorFamily.SensorLECallibri, SensorFamily.SensorLEKolibri);
scanner.EventSensorsChanged += OnDeviceFound;
scanner.Start();
Thread.Sleep(15000);
Console.WriteLine("Cancel search");
scanner.Stop();
scanner.EventSensorsChanged -= OnDeviceFound;

/*** Connect to first sensor ***/
if (scanner.Sensors.Count > 0)
{
    SensorInfo sensorInfo = scanner.Sensors[0];
    Console.WriteLine($"Connect to {sensorInfo.Name} ({sensorInfo.Address})");
    CallibriSensor? sensor = scanner.CreateSensor(sensorInfo) as CallibriSensor;

    sensor.SignalTypeCallibri = CallibriSignalType.EEG;
    sensor.SamplingFrequency = SensorSamplingFrequency.FrequencyHz1000;
    sensor.ExtSwInput = SensorExternalSwitchInput.ExtSwInUSB;
    sensor.HardwareFilters = [SensorFilter.FilterHPFBwhLvl1CutoffFreq1Hz];

    emotionController.StartCalibration();

    Console.WriteLine($"Start signal in V for a minute:");
    sensor.EventCallibriSignalDataRecived += Sensor_EventCallibriSignalDataRecived;
    sensor.ExecCommand(SensorCommand.CommandStartSignal);
    Thread.Sleep(60 * 1000);
    sensor.ExecCommand(SensorCommand.CommandStopSignal);
    sensor.EventCallibriSignalDataRecived -= Sensor_EventCallibriSignalDataRecived;

    sensor.Dispose();
    sensor = null;
}

/*** Finish work ***/
emotionController.Dispose();

void Sensor_EventCallibriSignalDataRecived(ISensor sensor, CallibriSignalData[] data)
{
    string result = emotionController.PushData(data.SelectMany(samples => samples.Samples).ToArray());
    Console.WriteLine($"Emotion data: {result}");
}

void OnDeviceFound(IScanner scanner, IReadOnlyList<SensorInfo> sensors)
{
    Console.WriteLine($"Found {sensors.Count} devices");
    foreach (var sensor in sensors)
    {
        Console.WriteLine($"With name {sensor.Name} and adress {sensor.Address}");
    }
}