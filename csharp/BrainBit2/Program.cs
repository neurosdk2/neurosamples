﻿using NeuroSDK;

Console.WriteLine("Search devices (15s):");
Scanner scanner = new Scanner(SensorFamily.SensorLEBrainBit2, SensorFamily.SensorLEBrainBitPro, SensorFamily.SensorLEBrainBitFlex);
scanner.EventSensorsChanged += onDeviceFound;
scanner.Start();
Thread.Sleep(15000);
Console.WriteLine("Cancel search");
scanner.Stop();
scanner.EventSensorsChanged -= onDeviceFound;


foreach (SensorInfo sens in scanner.Sensors)
{
    Console.WriteLine($"Connect to {sens.Name} ({sens.Address})");
    BrainBit2Sensor? sensor = scanner.CreateSensor(sens) as BrainBit2Sensor;
    if (sensor != null)
    {
        Console.WriteLine($"Successfully connected to {sens.Name}!");

        Console.WriteLine($"Set gain...");
        BrainBit2AmplifierParam? ampParams = sensor?.AmplifierParamBrainBit2;
        Array.Fill(ampParams?.ChGain, SensorGain.SensorGain3);
        sensor.AmplifierParamBrainBit2 = ampParams ?? new BrainBit2AmplifierParam();

        sensor.EventBatteryChanged += Sensor_EventBatteryChanged;
        sensor.EventSensorStateChanged += Sensor_EventSensorStateChanged;

        var features = sensor?.Features;
        Console.WriteLine($"{sensor?.Name} features:");
        foreach (var feature in features)
        {
            Console.WriteLine(feature.ToString());
        }

        var commands = sensor?.Commands;
        Console.WriteLine($"{sensor?.Name} commands: ");
        foreach (var command in commands)
        {
            Console.WriteLine(command.ToString());
        }

        var parameters = sensor?.Parameters;
        Console.WriteLine($"{sensor.Name} parameters:");
        foreach (ParameterInfo parameter in parameters)
        {
            switch (parameter.Param)
            {
                case SensorParameter.ParameterName:
                    Console.WriteLine($"Name: {sensor?.Name} (Access: {parameter.ParamAccess})");
                    break;
                case SensorParameter.ParameterState:
                    Console.WriteLine($"Connection state: {sensor?.State} (Access: {parameter.ParamAccess})");
                    break;
                case SensorParameter.ParameterAddress:
                    Console.WriteLine($"Address: {sensor?.Address} (Access: {parameter.ParamAccess})");
                    break;
                case SensorParameter.ParameterSerialNumber:
                    Console.WriteLine($"Serial number: {sensor?.SerialNumber} (Access: {parameter.ParamAccess})");
                    break;
                case SensorParameter.ParameterFirmwareMode:
                    Console.WriteLine($"Firmware mode: {sensor?.FirmwareMode} (Access: {parameter.ParamAccess})");
                    break;
                case SensorParameter.ParameterSamplingFrequency:
                    Console.WriteLine($"Sampling frequency: {sensor?.SamplingFrequency} (Access: {parameter.ParamAccess})");
                    break;
                case SensorParameter.ParameterGain:
                    Console.WriteLine($"Gain: {sensor?.Gain} (Access: {parameter.ParamAccess})");
                    break;
                case SensorParameter.ParameterOffset:
                    Console.WriteLine($"Offset: {sensor?.DataOffset} (Access: {parameter.ParamAccess})");
                    break;
                case SensorParameter.ParameterFirmwareVersion:
                    Console.WriteLine($"Sensor version (Access: {parameter.ParamAccess}): "
                                + $"[FW]: {sensor?.Version.FwMajor}.{sensor?.Version.FwMinor} "
                                + $"[HW]: {sensor?.Version.HwMajor}.{sensor?.Version.HwMinor}.{sensor?.Version.HwPatch} "
                                + $"[Ext]: {sensor?.Version.ExtMajor}");
                    break;
                case SensorParameter.ParameterBattPower:
                    Console.WriteLine($"Battery: {sensor?.BattPower} (Access: {parameter.ParamAccess})");
                    break;
                case SensorParameter.ParameterSensorFamily:
                    Console.WriteLine($"Sensor family: {sensor?.SensFamily} (Access: {parameter.ParamAccess})");
                    break;
                case SensorParameter.ParameterSensorMode:
                    Console.WriteLine($"Sensor mode (Access: {parameter.ParamAccess}): {sensor?.FirmwareMode}");
                    break;
                case SensorParameter.ParameterAmplifier:
                    BrainBit2AmplifierParam? showAmpParams = sensor?.AmplifierParamBrainBit2;
                    Console.WriteLine($"Amlifier params: ");
                    Console.WriteLine($"Current: {showAmpParams?.Current}, Gain = {showAmpParams?.ChGain[0]}, Signal mode = {showAmpParams?.ChSignalMode[0]}, Resist use = {showAmpParams?.ChResistUse}");
                    break;
                default:
                    break;
            }
        }

        Console.WriteLine($"Resistance (10s) in V:");
        sensor.EventBrainBit2ResistDataRecived += Sensor_EventBrainBit2ResistDataRecived;
        sensor.ExecCommand(SensorCommand.CommandStartResist);
        Thread.Sleep(10000);
        sensor.ExecCommand(SensorCommand.CommandStopResist);
        sensor.EventBrainBit2ResistDataRecived -= Sensor_EventBrainBit2ResistDataRecived;

        Console.WriteLine($"Signal (10s) in V:");
        sensor.EventBrainBit2SignalDataRecived += Sensor_EventBrainBit2SignalDataRecived;
        sensor.ExecCommand(SensorCommand.CommandStartSignal);
        Thread.Sleep(10000);
        sensor.ExecCommand(SensorCommand.CommandStopSignal);
        sensor.EventBrainBit2SignalDataRecived -= Sensor_EventBrainBit2SignalDataRecived;


        Console.WriteLine($"Disconnecting from {sensor.Name}...");
        sensor.Disconnect();
        Console.WriteLine($"And remove device...");
        sensor.Dispose();
        sensor = null;
    }
}

void Sensor_EventBrainBit2SignalDataRecived(ISensor sensor, SignalChannelsData[] data)
{
    foreach( SignalChannelsData sample in data)
    {
        string dataOut = $"PackNum = {sample.PackNum}, Marker = {sample.Marker}, Samples = ";
        foreach (double signalSample in sample.Samples)
        {
            dataOut += $"{signalSample}, ";
        }
        Console.WriteLine(dataOut);
    }
}

void Sensor_EventBrainBit2ResistDataRecived(ISensor sensor, ResistRefChannelsData[] data)
{
    foreach (ResistRefChannelsData sample in data)
    {
        string dataOut = $"PackNum = {sample.PackNum}, Resist samples = ";
        foreach (double signalSample in sample.Samples)
        {
            dataOut += $"{signalSample}, ";
        }
        Console.WriteLine(dataOut);
    }
}

void onDeviceFound(IScanner scanner, IReadOnlyList<SensorInfo> sensors)
{
    Console.WriteLine($"Found {sensors.Count} devices");
    foreach (var sensor in sensors)
    {
        Console.WriteLine($"With name {sensor.Name} and adress {sensor.Address}");
    }
}

void Sensor_EventSensorStateChanged(ISensor sensor, SensorState sensorState)
{
    Console.WriteLine($"{sensor.Name} connection state is {sensorState}");
}

void Sensor_EventBatteryChanged(ISensor sensor, int battPower)
{
    Console.WriteLine($"{sensor.Name} battery is {battPower}");
}

scanner.Dispose();