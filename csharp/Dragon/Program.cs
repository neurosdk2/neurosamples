﻿using NeuroSDK;

Console.WriteLine("Search devices (15s):");
Scanner scanner = new Scanner(SensorFamily.SensorLENeuroEEG);
scanner.EventSensorsChanged += onDeviceFound;
scanner.Start();
Thread.Sleep(15000);
Console.WriteLine("Cancel search");
scanner.Stop();
scanner.EventSensorsChanged -= onDeviceFound;

string[] ChannelNames = [];

foreach (SensorInfo sens in scanner.Sensors)
{
    Console.WriteLine($"Connect to {sens.Name} ({sens.Address})");
    NeuroEEGSensor? sensor = scanner.CreateSensor(sens) as NeuroEEGSensor;
    if (sensor != null)
    {
        Console.WriteLine($"Successfully connected to {sens.Name}!");

        sensor.EventBatteryChanged += Sensor_EventBatteryChanged;
        sensor.EventSensorStateChanged += Sensor_EventSensorStateChanged;
        sensor.EventSensorAmpModeChanged += Sensor_EventSensorAmpModeChanged;

        /* Set amplifier parameters */
        var ampParam = sensor.AmplifierParamNeuroEEG;
        ampParam.Frequency = SensorSamplingFrequency.FrequencyHz500;
        ampParam.ReferentMode = EEGRefMode.RefA1A2;
        ampParam.ReferentResistMesureAllow = true;
        for (int i = 0; i < ampParam.ChannelGain.Length; ++i)
            ampParam.ChannelGain[i] = SensorGain.SensorGain6;
        for (int i = 0; i < ampParam.ChannelMode.Length; ++i)
            ampParam.ChannelMode[i] = EEGChannelMode.EEGChModeSignalResist;
        sensor.AmplifierParamNeuroEEG = ampParam;

        /* Read parameters */
        var features = sensor?.Features;
        Console.WriteLine($"{sensor?.Name} features:");
        foreach (var feature in features)
        {
            Console.WriteLine(feature.ToString());
        }

        var commands = sensor?.Commands;
        Console.WriteLine($"{sensor?.Name} commands: ");
        foreach (var command in commands)
        {
            Console.WriteLine(command.ToString());
        }

        Console.WriteLine($"{sensor?.Name} channels: ");
        ChannelNames = new string[sensor.ChannelsCount];
        foreach (var channel in sensor?.SupportedChannelsNeuroEEG)
        {
            Console.WriteLine($"Name={channel.Name} Id={channel.Id} Num={channel.Num}");
            ChannelNames[channel.Num] = channel.Name;
        }

        var parameters = sensor?.Parameters;
        Console.WriteLine($"{sensor.Name} parameters:");
        foreach (ParameterInfo parameter in parameters)
        {
            switch (parameter.Param)
            {
                case SensorParameter.ParameterName:
                    Console.WriteLine($"Name: {sensor?.Name} (Access: {parameter.ParamAccess})");
                    break;
                case SensorParameter.ParameterState:
                    Console.WriteLine($"Connection state: {sensor?.State} (Access: {parameter.ParamAccess})");
                    break;
                case SensorParameter.ParameterAddress:
                    Console.WriteLine($"Address: {sensor?.Address} (Access: {parameter.ParamAccess})");
                    break;
                case SensorParameter.ParameterSerialNumber:
                    Console.WriteLine($"Serial number: {sensor?.SerialNumber} (Access: {parameter.ParamAccess})");
                    break;
                case SensorParameter.ParameterFirmwareMode:
                    Console.WriteLine($"Firmware mode: {sensor?.FirmwareMode} (Access: {parameter.ParamAccess})");
                    break;
                case SensorParameter.ParameterSamplingFrequency:
                    Console.WriteLine($"Sampling frequency: {sensor?.SamplingFrequency} (Access: {parameter.ParamAccess})");
                    break;
                case SensorParameter.ParameterGain:
                    Console.WriteLine($"Gain: {sensor?.Gain} (Access: {parameter.ParamAccess})");
                    break;
                case SensorParameter.ParameterOffset:
                    Console.WriteLine($"Offset: {sensor?.DataOffset} (Access: {parameter.ParamAccess})");
                    break;
                case SensorParameter.ParameterFirmwareVersion:
                    Console.WriteLine($"Sensor version (Access: {parameter.ParamAccess}): "
                                + $"[FW]: {sensor?.Version.FwMajor}.{sensor?.Version.FwMinor} "
                                + $"[HW]: {sensor?.Version.HwMajor}.{sensor?.Version.HwMinor}.{sensor?.Version.HwPatch} "
                                + $"[Ext]: {sensor?.Version.ExtMajor}");
                    break;
                case SensorParameter.ParameterBattPower:
                    Console.WriteLine($"Battery: {sensor?.BattPower} (Access: {parameter.ParamAccess})");
                    break;
                case SensorParameter.ParameterSensorFamily:
                    Console.WriteLine($"Sensor family: {sensor?.SensFamily} (Access: {parameter.ParamAccess})");
                    break;
                case SensorParameter.ParameterSensorMode:
                    Console.WriteLine($"Sensor mode (Access: {parameter.ParamAccess}): {sensor?.FirmwareMode}");
                    break;
                default:
                    break;
            }
        }

        Console.WriteLine($"Signal (5s) in mV:");
        sensor.EventNeuroEEGSignalDataRecived += Sensor_EventNeuroEEGSignalDataRecived; ;
        sensor.ExecCommand(SensorCommand.CommandStartSignal);
        Thread.Sleep(5000);
        sensor.ExecCommand(SensorCommand.CommandStopSignal);
        sensor.EventNeuroEEGSignalDataRecived -= Sensor_EventNeuroEEGSignalDataRecived;

        Console.WriteLine($"Resist (5s) in mV:");
        sensor.EventNeuroEEGResistDataRecived += Sensor_EventNeuroEEGResistDataRecived;
        sensor.ExecCommand(SensorCommand.CommandStartResist);
        Thread.Sleep(5000);
        sensor.ExecCommand(SensorCommand.CommandStopResist);
        sensor.EventNeuroEEGResistDataRecived -= Sensor_EventNeuroEEGResistDataRecived;

        Console.WriteLine($"Disconnecting from {sensor.Name}...");
        sensor.Disconnect();
        sensor.EventBatteryChanged -= Sensor_EventBatteryChanged;
        sensor.EventSensorStateChanged -= Sensor_EventSensorStateChanged;
        Console.WriteLine($"And remove device...");
        sensor.Dispose();
        sensor = null;
    }
}

void Sensor_EventSensorAmpModeChanged(ISensor sensor, SensorAmpMode sensorAmpMode)
{
    Console.WriteLine($"Sensor mode = {sensorAmpMode}");
}

void Sensor_EventNeuroEEGSignalDataRecived(ISensor sensor, SignalChannelsData[] data)
{
    if (data == null)
        return;
    foreach (var it in data)
    {
        if (it.Samples == null)
            continue;
        string sLine = $"[{it.PackNum}]";
        for (int i = 0; i < it.Samples.Length; i++)
            sLine = sLine + $"[{ChannelNames[i]}: {it.Samples[i]}]";
        Console.WriteLine(sLine);
    }
}

void Sensor_EventNeuroEEGResistDataRecived(ISensor sensor, ResistChannelsData[] data)
{
    if (data == null)
        return;
    foreach (var it in data)
    {
        if (it.Values == null)
            continue;
        string sLine = $"[PackNum={it.PackNum}] [A1={it.A1}] [A2={it.A2}] [Bias={it.Bias}]";
        foreach (var itValue in it.Values)
            sLine = sLine + $"[{ChannelNames[i]}: {itValue}]";
        Console.WriteLine(sLine);
    }
}


void onDeviceFound(IScanner scanner, IReadOnlyList<SensorInfo> sensors)
{
    Console.WriteLine($"Found {sensors.Count} devices");
    foreach (var sensor in sensors)
    {
        Console.WriteLine($"With name {sensor.Name} and adress {sensor.Address}");
    }
}

void Sensor_EventSensorStateChanged(ISensor sensor, SensorState sensorState)
{
    Console.WriteLine($"{sensor.Name} connection state is {sensorState}");
}

void Sensor_EventBatteryChanged(ISensor sensor, int battPower)
{
    Console.WriteLine($"{sensor.Name} battery is {battPower}");
}

scanner.Dispose();