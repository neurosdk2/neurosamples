﻿using NeuroSDK;
using Neurotech.CallibriUtils;
using System.Collections.Concurrent;

/*** Create ECG lib ***/
int samplingRate = 1000; // same as sensor.SamplingFrequency
int dataWindow = samplingRate / 2;
var nwinsForPressureIndex = 30;
CallibriMath math = new(samplingRate, dataWindow, nwinsForPressureIndex);

int packSize = samplingRate / 10;
ConcurrentQueue<double> samplesBuf = new();

/*** Search sensor ***/
Console.WriteLine("Search devices (15s):");
Scanner scanner = new Scanner(SensorFamily.SensorLECallibri, SensorFamily.SensorLEKolibri);
scanner.EventSensorsChanged += OnDeviceFound;
scanner.Start();
Thread.Sleep(15000);
Console.WriteLine("Cancel search");
scanner.Stop();
scanner.EventSensorsChanged -= OnDeviceFound;

/*** Connect to first sensor ***/
if(scanner.Sensors.Count > 0)
{
    SensorInfo sensorInfo = scanner.Sensors[0];
    Console.WriteLine($"Connect to {sensorInfo.Name} ({sensorInfo.Address})");
    CallibriSensor? sensor = scanner.CreateSensor(sensorInfo) as CallibriSensor;

    sensor.SignalTypeCallibri = CallibriSignalType.ECG;
    sensor.SamplingFrequency = SensorSamplingFrequency.FrequencyHz1000;

    sensor.ExtSwInput = SensorExternalSwitchInput.ExtSwInElectrodes;
    sensor.HardwareFilters = [SensorFilter.FilterHPFBwhLvl1CutoffFreq1Hz,
                    SensorFilter.FilterBSFBwhLvl2CutoffFreq45_55Hz,
                    SensorFilter.FilterBSFBwhLvl2CutoffFreq55_65Hz];


    Console.WriteLine($"Start signal in V for a minute:");
    sensor.EventCallibriSignalDataRecived += Sensor_EventCallibriSignalDataRecived;
    sensor.ExecCommand(SensorCommand.CommandStartSignal);
    Thread.Sleep(60 * 1000);
    sensor.ExecCommand(SensorCommand.CommandStopSignal);
    sensor.EventCallibriSignalDataRecived -= Sensor_EventCallibriSignalDataRecived;

    sensor.Dispose();
    sensor = null;
}

/*** Finish work ***/
math.Dispose();


void Sensor_EventCallibriSignalDataRecived(ISensor sensor, CallibriSignalData[] data)
{
    ProcessSamples(data.SelectMany(samples => samples.Samples).ToArray());
}

void ProcessSamples(IEnumerable<double> samples)
{
    foreach (double x in samples)
    {
        samplesBuf.Enqueue(x * 1e6);
    }

    var pack = new double[packSize];

    if (samplesBuf.Count >= packSize)
        for (var i = 0; i < packSize; i++)
            samplesBuf.TryDequeue(out pack[i]);
    else return;

    math.PushData(pack);

    bool rrDetected = math.RRdetected;
    if (!rrDetected) return;

    Console.WriteLine($"RR: {math.RR}");
    Console.WriteLine($"HR: {math.HR}");
    Console.WriteLine($"AmplModa: {math.AmplModa}");
    Console.WriteLine($"Moda: {math.Moda}");
    Console.WriteLine($"PressureIndex: {math.PressureIndex}");
    Console.WriteLine($"VariationDist: {math.VariationDist}");

    math.SetRRchecked();
}


void OnDeviceFound(IScanner scanner, IReadOnlyList<SensorInfo> sensors)
{
    Console.WriteLine($"Found {sensors.Count} devices");
    foreach (var sensor in sensors)
    {
        Console.WriteLine($"With name {sensor.Name} and adress {sensor.Address}");
    }
}