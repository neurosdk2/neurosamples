﻿namespace BrainBitDemo2.Pages.Elements;

internal class Point
{
    public float Left   { get; set; }
    public float Right  { get; set; }
    public float Top    { get; set; }
    public float Bottom { get; set; }

    public Point(float x, float y)
    {
        Left   = x;
        Right  = x;
        Top    = y;
        Bottom = y;
    }

    public Point(float left, float top, float right, float bottom)
    {
        Left = left;
        Right = right;
        Top = top;
        Bottom = bottom;
    }

}
