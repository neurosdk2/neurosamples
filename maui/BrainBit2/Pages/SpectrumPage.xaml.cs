﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using BrainBitDemo2.NeuroImpl;
using BrainBitDemo2.Pages.Elements;
using NeuroSDK;

using NeuroTech.Spectrum;

namespace BrainBitDemo2.Pages;

public partial class SpectrumPage : ContentPage
{
    private readonly Dictionary<int, SpectrumController> _controllers = new();
    private Dictionary<int, SpectrumGraph> _charts = new();

    public string SpectrumButtonText { get => IsStarted ? "Pause" : "Start"; }

    private bool _isStarted;

    private bool IsStarted
    {
        get => _isStarted;

        set
        {
            if (_isStarted == value) return;

            _isStarted = value;
            OnPropertyChanged(nameof(SpectrumButtonText));
        }
    }

    public SpectrumPage()
    {
        InitializeComponent();

        BindingContext = this;

        int samplingFrequencyHz = BrainBitController.Instance.SamplingFrequency;
        foreach(var chInf in BrainBitController.Instance.chInfs)
        {
            _controllers.Add(chInf.Num, new SpectrumController(samplingFrequencyHz, chInf.Num));
            SpectrumGraph graph = new()
            {
                ChNum = chInf.Num,
                HorizontalOptions = LayoutOptions.Fill,
                VerticalOptions = LayoutOptions.Fill
            };
            _charts.Add(chInf.Num, graph);
        }

        BrainBitController.Instance.SignalReceived = OnSignalReceived;

        InitCharts();
    }

    protected override void OnAppearing()
    {
        base.OnAppearing();

        foreach (var controller in _controllers.Values)
            controller.ProcessedData = OnProcessedData;

        BrainBitController.Instance.ConnectionStateChanged = DevStateView.ConnectionStateChanged;
        BrainBitController.Instance.BatteryChanged         = DevStateView.BatteryChanged;

        DevStateView.ConnectionStateChanged(null, BrainBitController.Instance.ConnectionState);
        DevStateView.BatteryChanged(null, BrainBitController.Instance.BatteryPower);
    }

    protected override void OnDisappearing()
    {
        BrainBitController.Instance.StopSignal();

        BrainBitController.Instance.SignalReceived = null;

        foreach (var controller in _controllers.Values)
            controller.ProcessedData = null;

        foreach (var controller in _controllers.Values)
            controller.Dispose();   

        _controllers.Clear();

        base.OnDisappearing();
    }

    private void OnSignalReceived(ISensor sensor, SignalChannelsData[] signalData)
    {
        foreach (var controller in _controllers)
        {
            var s = signalData.Select(sample => sample.Samples[controller.Key]).ToArray();
            controller.Value.ProcessSamples(s);
        }
    }

    private void OnProcessedData(IReadOnlyCollection<RawSpectrumData> rawSpectrumData, IReadOnlyCollection<WavesSpectrumData> wavesSpectrumData, int chNum)
    {
        if (wavesSpectrumData.Count > 0)
        {
            double avgAlphaRaw = wavesSpectrumData.Select(it => it.Alpha_Raw).Average();
            double avgBetaRaw  = wavesSpectrumData.Select(it => it.BetaRaw).Average();
            double avgGammaRaw = wavesSpectrumData.Select(it => it.GammaRaw).Average();
            double avgDeltaRaw = wavesSpectrumData.Select(it => it.DeltaRaw).Average();
            double avgThetaRaw = wavesSpectrumData.Select(it => it.ThetaRaw).Average();

            double avgAlphaRel = wavesSpectrumData.Select(it => it.Alpha_Rel).Average();
            double avgBetaRel  = wavesSpectrumData.Select(it => it.BetaRel).Average();
            double avgGammaRel = wavesSpectrumData.Select(it => it.GammaRel).Average();
            double avgDeltaRel = wavesSpectrumData.Select(it => it.DeltaRel).Average();
            double avgThetaRel = wavesSpectrumData.Select(it => it.ThetaRel).Average();

            string wavesDataText = $"\nChannel {chNum}:\n"
                                 + $"Alpha Raw: {avgAlphaRaw:F}\n"
                                 + $"Beta Raw: {avgBetaRaw:F}\n"
                                 + $"Gamma Raw: {avgGammaRaw:F}\n"
                                 + $"Delta Raw: {avgDeltaRaw:F}\n"
                                 + $"Theta Raw: {avgThetaRaw:F}\n\n"
                                 + $"Alpha Rel: {avgAlphaRel:F}\n"
                                 + $"Beta Rel: {avgBetaRel:F}\n"
                                 + $"Gamma Rel: {avgGammaRel:F}\n"
                                 + $"Delta Rel: {avgDeltaRel:F}\n"
                                 + $"Theta Rel: {avgThetaRel:F}\n";

            Debug.WriteLine(wavesDataText, nameof(SpectrumPage));
        }

        if (rawSpectrumData.Count <= 0) return;

        foreach (RawSpectrumData spectrumData in rawSpectrumData.Where(it => it.AllBinsValues.Length != 0)) _charts[chNum].Entries = spectrumData.AllBinsValues;
    }

    private void InitCharts()
    {

        ChartsGrid.ColumnDefinitions = new()
        {
            new ColumnDefinition { Width = new GridLength(0.5, GridUnitType.Star) },
            new ColumnDefinition { Width = new GridLength(0.5, GridUnitType.Star) },
        };

        double rowCount = _controllers.Count / 2;

        int chCount = 0;
        for (int i = 0; i < rowCount; i++)
        {
            ChartsGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1.0 / rowCount, GridUnitType.Star) });
            for (int j = 0; j < 2; j++)
            {
                ChartsGrid.Add(_charts[chCount], j, i);
                _charts[chCount].Init(1);
                chCount++;
            }
            
        }
    }

    private void SpectrumButton_OnClicked(object sender, EventArgs e)
    {
        IsStarted = !IsStarted;

        if (IsStarted)
        {
            foreach (var controller in _controllers.Values)
                controller.ProcessedData = OnProcessedData;

            BrainBitController.Instance.StartSignal();
        }
        else
        {
            foreach (var controller in _controllers.Values)
                controller.ProcessedData = null;

            BrainBitController.Instance.StopSignal();
        }
    }
}
