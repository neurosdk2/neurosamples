﻿using BrainBitDemo2.NeuroImpl;

using NeuroSDK;

namespace BrainBitDemo2.Pages;

public partial class ResistancePage : ContentPage
{
    private Dictionary<int, string> _chIds = new();

    private string _resistanceText = "";

    public string ResistanceText
    {
        get => _resistanceText;

        set
        {
            if (_resistanceText == value) return;

            _resistanceText = value;
            OnPropertyChanged();
        }
    }

    public ResistancePage()
    {
        InitializeComponent();
        BindingContext = this;

        foreach (EEGChannelInfo ch in BrainBitController.Instance.chInfs)
            _chIds.Add(ch.Num, ch.Name);
    }

    protected override void OnAppearing()
    {
        base.OnAppearing();

        BrainBitController.Instance.ConnectionStateChanged = DevStateView.ConnectionStateChanged;
        BrainBitController.Instance.BatteryChanged         = DevStateView.BatteryChanged;

        DevStateView.ConnectionStateChanged(null, BrainBitController.Instance.ConnectionState);
        DevStateView.BatteryChanged(null, BrainBitController.Instance.BatteryPower);

        BrainBitController.Instance.ResistReceived = OnResistReceived;

        BrainBitController.Instance.StartResist();
    }

    protected override void OnDisappearing()
    {
        BrainBitController.Instance.StopResist();
        base.OnDisappearing();
    }

    private void OnResistReceived(ISensor sensor, ResistRefChannelsData[] data)
    {
        ResistRefChannelsData last = data[^1];

        string tmp = "";

        for (int i = 0; i < last.Samples.Length; i++)
        {
            tmp += $"{_chIds[i]}: {last.Samples[i]}\n";
        }

        ResistanceText = tmp;
    }
}
