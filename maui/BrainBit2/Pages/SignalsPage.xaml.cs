﻿using System;
using System.Collections.Generic;
using System.Linq;

using BrainBitDemo2.NeuroImpl;
using BrainBitDemo2.Pages.ChooseFilters;
using BrainBitDemo2.Pages.Elements;
using NeuroSDK;
using Neurotech.Filters;

namespace BrainBitDemo2.Pages;

public partial class SignalsPage : ContentPage
{
    #region Filters init

    private Dictionary<int, FilterList> _currentFilterList = new();
    public Dictionary<int, FilterList> CurrentFilterList
    {
        get
        {
            lock (_lockObj) return _currentFilterList;
        }

        set
        {
            lock (_lockObj) _currentFilterList = value;
        }
    }
    private readonly object _lockObj = new();

    #endregion

    public string SignalButtonText { get => _isStarted ? "Pause" : "Start"; }

    private bool _isStarted;

    public bool IsStarted
    {
        get => _isStarted;

        set
        {
            _isStarted = value;
            OnPropertyChanged(nameof(SignalButtonText));
        }
    }

    private List<IIRFilterParam> _selectedFilters;
    private Dictionary<int, SignalGraph> _charts = new();
    private List<EEGChannelInfo> _chInfos = new();

    public SignalsPage()
    {
        InitializeComponent();

        BindingContext = this;

        InitChart();
    }

    private void InitChart()
    {
        int sf = BrainBitController.Instance.SamplingFrequency;

        var chTmp = BrainBitController.Instance.chInfs;

        int chCount = 0;
        foreach (var chInf in chTmp)
        {
            SignalGraph graph = new()
            {
                HorizontalOptions = LayoutOptions.Fill,
                VerticalOptions = LayoutOptions.Fill,
            };
            graph.Init(5, sf, chInf.Name);
            graph.Amplitude = 10000 / 1e6f;
            ChartsSGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1.0 / chTmp.Count, GridUnitType.Star) });
            ChartsSGrid.Add(graph, 0, chCount);
            _charts.Add(chInf.Num, graph);
            chCount++;
        }
    }

    protected override void OnSizeAllocated(double width, double height)
    {
        base.OnSizeAllocated(width, height);

        
    }

    private void StartChart()
    {
        IsStarted                                  = true;
        BrainBitController.Instance.SignalReceived = OnSignalReceived;

        BrainBitController.Instance.StartSignal();
        foreach(var chart in _charts.Values)
            chart.StartAnimation();
    }

    private void StopChart()
    {
        BrainBitController.Instance.StopSignal();
        foreach (var chart in _charts.Values)
            chart.StopAnimation();
    }

    protected override void OnAppearing()
    {
        base.OnAppearing();
        StartChart();

        BrainBitController.Instance.ConnectionStateChanged = DevStateView.ConnectionStateChanged;
        BrainBitController.Instance.BatteryChanged         = DevStateView.BatteryChanged;

        DevStateView.ConnectionStateChanged(null, BrainBitController.Instance.ConnectionState);
        DevStateView.BatteryChanged(null, BrainBitController.Instance.BatteryPower);
    }

    protected override void OnDisappearing()
    {
        BrainBitController.Instance.StopSignal();
        base.OnDisappearing();
    }

    private void OnSignalReceived(ISensor sensor, SignalChannelsData[] data)
    {
        foreach (var chart in _charts)
        {
            double[] values = data.Select(sample => sample.Samples[chart.Key]).ToArray();
            if (CurrentFilterList.ContainsKey(chart.Key))
            {
                CurrentFilterList[chart.Key]?.FilterArray(values);
            }
            chart.Value.AddSamples(values);
        }
    }

    private void SignalButton_Clicked(object sender, EventArgs e)
    {
        IsStarted = !IsStarted;

        if (IsStarted)
            StartChart();
        else
            StopChart();
    }

    private async void Filters_Clicked(object sender, EventArgs e)
    {
        var filtersPopUp = new ChooseFiltersPage(_selectedFilters);
        _selectedFilters = await filtersPopUp.Show(Navigation);

        CurrentFilterList.Clear();

        if (_selectedFilters is { Count: > 0 })
        {
            foreach(var chart in _charts)
            {
                var newFilterList = new FilterList();
                foreach (IIRFilterParam item in _selectedFilters) newFilterList.AddFilter(new IIRFilter(item));
                CurrentFilterList[chart.Key] = newFilterList;
            }
        }
    }

}
