﻿using System;

namespace BrainBitDemo2.Utils;

public interface ISensorHelper
{
    Task<bool> EnableSensor();
}
