﻿using System;
using System.Diagnostics;
using NeuroTech.Spectrum;

namespace BrainBitDemo2.NeuroImpl;

public class SpectrumController
{
    private const double AlphaCoef = 1.0;
    private const double BetaCoef  = 1.0;
    private const double DeltaCoef = 0.0;
    private const double GammaCoef = 0.0;
    private const double ThetaCoef = 1.0;

    private const int ProcessWinRate = 10; // Hz
    private const int BordFrequency  = 250 / 2;
    private const int FftWindow      = 250 * 1;

    public Action<RawSpectrumData[], WavesSpectrumData[], int> ProcessedData;

    private SpectrumMath _math;

    private readonly int _channelNum;

    public SpectrumController(int samplingRate, int channelNum)
    {
        _math = new SpectrumMath(samplingRate, FftWindow, ProcessWinRate);
        _math.InitParams(BordFrequency, true);
        _math.SetWavesCoeffs(DeltaCoef, ThetaCoef, AlphaCoef, BetaCoef, GammaCoef);

        _channelNum = channelNum;
    }

    public void Dispose() { _math.Dispose(); }

    public double FftBinsFor1Hz { get => _math.GetFFTBinsFor1Hz(); }

    public void ProcessSamples(double[] samples)
    {
        try
        {
            Trace.WriteLine($"S count: {samples.Length}");
            _math.PushData(samples);

            var rawSpectrumData = _math.ReadRawSpectrumInfoArr();
            var wavesSpectrumData = _math.ReadWavesSpectrumInfoArr();

            _math.SetNewSampleSize();

            ProcessedData?.Invoke(rawSpectrumData, wavesSpectrumData, _channelNum);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
    }
}
