﻿namespace BrainBitDemo2.NeuroImpl;

public enum ChannelType
{
    O1,
    O2,
    T3,
    T4
}
