﻿using NeuroSDK;

using SignalMath;

namespace BrainBitDemo2.NeuroImpl;

public class EmotionsMonopolarController
{
    private readonly List<EegEmotionalMath> _math = new();

    public Action<int, string> progressCalibrationCallback = null;
    public Action<bool, string> isArtefactedSequenceCallback = null;
    public Action<bool, string> isBothSidesArtifactedCallback = null;
    public Action<SpectralDataPercents, string> lastSpectralDataCallback = null;
    public Action<RawSpectVals, string> rawSpectralDataCallback = null;
    public Action<MindData, string> lastMindDataCallback = null;

    private bool isCalibrated = false;

    private object _locker = new object();

    private IReadOnlyList<EEGChannelInfo> _infos;


    public EmotionsMonopolarController() 
    {
        var config = EmotionalMathConfig.GetDefault(false);

        _infos = BrainBitController.Instance.chInfs;

        foreach (var ch in _infos)
        {
            _math.Add(new EegEmotionalMath(
                config.MathLib,
                config.ArtifactDetect,
                config.ShortArtifactDetect,
                config.MentalAndSpectral));
        }

        foreach (var math in _math)
        {
            math?.SetZeroSpectWaves(config.Active, config.delta, config.theta, config.alpha, config.beta, config.gamma);
            math?.SetWeightsForSpectra(config.delta_c, config.theta_c, config.alpha_c, config.beta_c, config.gamma_c);
            math?.SetCallibrationLength(config.CallibrationLength);
            math?.SetMentalEstimationMode(config.MentalEstimation);
            math?.SetPrioritySide(config.PrioritySide);
            math?.SetSkipWinsAfterArtifact(config.SkipWinsAfterArtifact);
            math?.SetSpectNormalizationByBandsWidth(config.SpectNormalizationByBandsWidth);
            math?.SetSpectNormalizationByCoeffs(config.SpectNormalizationByCoeffs);
        }
        
    }

    public void Dispose() 
    {
        lock (_locker)
        {
            foreach (var math in _math)
            {
                math?.Dispose();
            }
            _math.Clear();
        }
        
    }

    public void StartCalibration() 
    {
        lock ( _locker ) {
            foreach (var math in _math)
            {
                math?.StartCalibration();
            }
        }
        
    }

    public void ProcessData(SignalChannelsData[] samples)
    {
        Task.Factory.StartNew(() => {
            lock (_locker)
            {
                List<RawChannelsArray[]> channels = new();
                foreach (var ch in _infos)
                {
                    channels.Add(new RawChannelsArray[samples[0].Samples.Length]);
                }

                for (int i = 0; i < samples.Length; i++)
                {
                    for (int j = 0; j < channels.Count; j++)
                    {
                        channels[j][i].channels = [samples[i].Samples[j]];
                    }
                }

                for(int i = 0; i < channels.Count; i++)
                {
                    _math[i]?.PushDataArr(channels[i]);
                }

                try
                {
                    foreach (var math in _math)
                    {
                        math?.ProcessDataArr();
                    }
                }
                catch { }

                resolveArtefacted();

                if (!isCalibrated)
                {
                    processCalibration();
                }
                else
                {
                    resolveSpectralData();
                    resolveRawSpectralData();
                    resolveMindData();
                }
            }
            
        });
    }

    private void resolveArtefacted()
    {
        for (int i = 0; i < _math.Count; i++)
        {
            // sequence artifacts
            bool isArtifactedSequence = _math[i].IsArtifactedSequence();
            isArtefactedSequenceCallback?.Invoke(isArtifactedSequence, _infos[i].Name);

            // both sides artifacts
            bool isBothSideArtifacted = _math[i].IsBothSidesArtifacted();
            isBothSidesArtifactedCallback?.Invoke(isBothSideArtifacted, _infos[i].Name);
        }
    }

    private void processCalibration()
    {
        for (int i = 0; i < _math.Count; i++)
        {
            isCalibrated = _math[i].CalibrationFinished();
            if (!isCalibrated)
            {
                int progress = _math[i].GetCallibrationPercents();
                progressCalibrationCallback?.Invoke(progress, _infos[i].Name);
            }
        }
    }

    private void resolveSpectralData() 
    {
        for (int i = 0; i < _math.Count; i++)
        {
            var spectralValues = _math[i].ReadSpectralDataPercentsArr();
            lastSpectralDataCallback?.Invoke(spectralValues.Last(), _infos[i].Name);
        }
    }
    private void resolveRawSpectralData() 
    {
        for (int i = 0; i < _math.Count; i++)
        {
            var rawSpectralValues = _math[i].ReadRawSpectralVals();
            rawSpectralDataCallback?.Invoke(rawSpectralValues, _infos[i].Name);
        }
    }
    private void resolveMindData() 
    {
        for (int i = 0; i < _math.Count; i++)
        {
            var mentalValues = _math[i].ReadMentalDataArr();
            lastMindDataCallback?.Invoke(mentalValues.Last(), _infos[i].Name);
        }
    }
}
