﻿using Microsoft.Extensions.Logging;
using Microsoft.Maui.Controls;
using SkiaSharp.Views.Maui.Controls.Hosting;
using BrainBitDemo2.Utils;

#if ANDROID
using XNeuroSDK.NET.Android;
using BrainBit2.Platforms.Android.Utils;
#endif

namespace BrainBit2
{
    public static class MauiProgram
    {
        public static MauiApp CreateMauiApp()
        {
            var builder = MauiApp.CreateBuilder();
            builder
                .UseMauiApp<App>()
                .UseSkiaSharp()
#if ANDROID
                .UseNeurosdk2()
#endif
                //.RegisterAppServices()
                .ConfigureFonts(fonts =>
                {
                    fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                    fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
                });

#if DEBUG
    		builder.Logging.AddDebug();
#endif
            return builder.Build();
        }

        public static MauiAppBuilder RegisterAppServices(this MauiAppBuilder mauiAppBuilder)
        {
#if ANDROID
            mauiAppBuilder.Services.AddSingleton<ISensorHelper, SensorHelper>();
#endif
            return mauiAppBuilder;
        }


    }
}
