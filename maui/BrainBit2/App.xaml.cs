﻿using BrainBitDemo2.Pages;

namespace BrainBit2
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());
        }
    }
}
