﻿namespace BrainBitDemo.NeuroImpl;

public enum ChannelType
{
    O1,
    O2,
    T3,
    T4
}
