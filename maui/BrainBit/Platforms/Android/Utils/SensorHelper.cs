﻿using Android.Bluetooth;
using Android.Content;
using Android.Locations;
using Android.OS;
using AndroidX.Activity.Result.Contract;
using AndroidX.Activity.Result;
using AndroidX.Core.Content;
using AndroidX.Core.Location;

using Native = Android.App;
using GMS = Android.Gms;
using Android.Gms.Common;

using AndroidX.AppCompat.App;
using Android;
using Android.Content.PM;
using BrainBitDemo.Utils;
using Android.Gms.Location;
using Android.Gms.Common.Apis;
using Android.Gms.Tasks;

namespace BrainBit.Platforms.Android.Utils
{
    public class SensorHelper : ISensorHelper
    {
        private readonly WeakReference<Context?> _currentActivity = new(null);
        public Context? ActivityContext
        {
            get => _currentActivity.TryGetTarget(out var a) ? a : null;
            set => _currentActivity.SetTarget(value!);
        }

        private ActivityForResult _activityForResult;
        private ActivityResultLauncher _activityForResultLauncher;

        private RequestMultiplePermissions _requestMultiplePermissions;
        private ActivityResultLauncher _requestMultiplePermissionsLauncher;

        private IntentSenderResult _intentSenderResult;
        private ActivityResultLauncher _intentSenderResultLauncher;

        public SensorHelper()
        {
            ActivityContext = Platform.CurrentActivity;
            AppCompatActivity? activity = ActivityContext as AppCompatActivity;

            _activityForResult = new ActivityForResult(activity!);
            _activityForResultLauncher = activity!.RegisterForActivityResult(new ActivityResultContracts.StartActivityForResult(), _activityForResult);

            _requestMultiplePermissions = new RequestMultiplePermissions(activity);
            _requestMultiplePermissionsLauncher = activity.RegisterForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), _requestMultiplePermissions);

            _intentSenderResult = new IntentSenderResult(activity);
            _intentSenderResultLauncher = activity.RegisterForActivityResult(new ActivityResultContracts.StartIntentSenderForResult(), _intentSenderResult);
        }
        public Task<bool> EnableSensor()
        {
            return Task<bool>.Factory.StartNew(RequestPermissions);
        }
        private bool permissionsGranted()
        {
            return Build.VERSION.SdkInt >= BuildVersionCodes.S ?
                (ContextCompat.CheckSelfPermission(Native.Application.Context, Manifest.Permission.BluetoothScan) == (int)Permission.Granted &&
                 ContextCompat.CheckSelfPermission(Native.Application.Context, Manifest.Permission.BluetoothConnect) == (int)Permission.Granted) :
                (ContextCompat.CheckSelfPermission(Native.Application.Context, Manifest.Permission.Bluetooth) == (int)Permission.Granted &&
                 ContextCompat.CheckSelfPermission(Native.Application.Context, Manifest.Permission.AccessFineLocation) == (int)Permission.Granted &&
                 ContextCompat.CheckSelfPermission(Native.Application.Context, Manifest.Permission.AccessCoarseLocation) == (int)Permission.Granted);
        }

        private bool RequestPermissions()
        {
            if (permissionsGranted())
            {
                return TurnOnBTAsync();
            }
            else
            {
                string[] permissions = Build.VERSION.SdkInt >= BuildVersionCodes.S ?
                    [Manifest.Permission.BluetoothScan, Manifest.Permission.BluetoothConnect] :
                    [Manifest.Permission.Bluetooth, Manifest.Permission.AccessFineLocation, Manifest.Permission.AccessCoarseLocation];

                _requestMultiplePermissions.BeginRequest();
                _requestMultiplePermissionsLauncher.Launch(permissions);
                if (_requestMultiplePermissions.WaitResult())
                {
                    return RequestPermissions();
                }
            }
            return false;
        }

        private bool TurnOnBTAsync()
        {
            BluetoothManager? BTManager = ActivityContext?.GetSystemService(Context.BluetoothService) as BluetoothManager;
            if (!BTManager!.Adapter!.IsEnabled)
            {
                _activityForResult.BeginRequest();
                _activityForResultLauncher.Launch(new Intent(BluetoothAdapter.ActionRequestEnable));
                if (_activityForResult.WaitResult())
                {
                    return TurnOnBTAsync();
                }
            }
            else
            {
                return TurnOnGpsAsync();
            }
            return false;
        }

        private bool TurnOnGpsAsync()
        {
            try
            {
                /*if(Build.VERSION.SdkInt >= BuildVersionCodes.S)
                {
                    ready.Set(true);
                    InvokeSensorsReady();
                    return;
                }*/

                LocationManager locationManager = (LocationManager)Native.Application.Context.GetSystemService(Context.LocationService);
                if (!LocationManagerCompat.IsLocationEnabled(locationManager!))
                {

                    if (isGooglePlayServicesAvailable(ActivityContext!))
                    {
                        var builder = new LocationSettingsRequest.Builder();
                        builder.AddLocationRequest(GMS.Location.LocationRequest.Create());
                        builder.SetAlwaysShow(false);
                        builder.SetNeedBle(true);

                        GPSOnCompleteListener GPSOn = new GPSOnCompleteListener();
                        var task = LocationServices
                            .GetSettingsClient(ActivityContext)
                            .CheckLocationSettings(builder.Build())
                            .AddOnCompleteListener(GPSOn);
                        try
                        {
                            while (!task.IsComplete)
                                Thread.Yield();
                            task.GetResult(Java.Lang.Class.FromType(typeof(ApiException)));
                        }
                        catch (ApiException ex)
                        {
                            try
                            {
                                var resolvable = ex as ResolvableApiException;
                                _intentSenderResult.BeginRequest();
                                _intentSenderResultLauncher.Launch(new IntentSenderRequest.Builder(resolvable!.Resolution).Build());
                                if (_intentSenderResult.WaitResult())
                                {
                                    if (LocationManagerCompat.IsLocationEnabled(locationManager!))
                                    {
                                        return true;
                                    }
                                    return false;
                                }
                            }
                            catch (Exception)
                            {
                            }
                        }
                        catch (Exception)
                        {
                        }
                        return false;
                    }
                    else
                    {
                        /* Intent intent = new Intent(Android.Provider.Settings.ActionLocationSourceSettings);
                         ((MainActivity)MainActivity.ActivityContext).StartActivity(intent);*/
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        private bool isGooglePlayServicesAvailable(Context context)
        {
            GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.Instance;
            int resultCode = googleApiAvailability.IsGooglePlayServicesAvailable(context);
            return resultCode == ConnectionResult.Success;
        }

        private class ActivityForResult : Java.Lang.Object, IActivityResultCallback
        {
            private readonly EventWaitHandle _taskWait = new EventWaitHandle(true, EventResetMode.ManualReset);
            private WeakReference<Native.Activity> _activity;
            private bool _state = false;
            public ActivityForResult(Native.Activity activity)
            {
                _activity = new WeakReference<Native.Activity>(activity);
            }
            public void OnActivityResult(Java.Lang.Object? activityResult)
            {
                _state = true;
                _taskWait.Set();
            }
            public bool WaitResult()
            {
                try
                {
                    while (!_state)
                    {
                        _taskWait.WaitOne(500);
                        if (_activity.TryGetTarget(out Native.Activity? activity) && activity == null)
                            return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
                return true;
            }

            public void BeginRequest()
            {
                try
                {
                    _state = false;
                    _taskWait.Reset();
                }
                catch (Exception)
                {
                }
            }
        }

        private class RequestMultiplePermissions : ActivityForResult
        {
            public RequestMultiplePermissions(Native.Activity activity) : base(activity)
            {
            }
        }

        private class IntentSenderResult : ActivityForResult
        {
            public IntentSenderResult(Native.Activity activity) : base(activity)
            {
            }
        }

        private class GPSOnCompleteListener : Java.Lang.Object, IOnCompleteListener
        {
            public bool Result { get; private set; }

            public void OnComplete(GMS.Tasks.Task task)
            {

            }

        }
    }

}
