﻿using System;

namespace BrainBitDemo.Utils;

public interface ISensorHelper
{
    Task<bool> EnableSensor();
}
