﻿#if ANDROID
using XNeuroSDK.NET.Android;
using BrainBit.Platforms.Android.Utils;
#endif
using Microsoft.Extensions.Logging;
using SkiaSharp.Views.Maui.Controls.Hosting;
using BrainBitDemo.Utils;

namespace BrainBit
{
    public static class MauiProgram
    {
        public static MauiApp CreateMauiApp()
        {
            var builder = MauiApp.CreateBuilder();
            builder
                .UseMauiApp<App>()
                .UseSkiaSharp()
                .ConfigureFonts(fonts =>
                {
                    fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                    fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
                });

#if ANDROID
            builder.UseNeurosdk2();
#endif

#if DEBUG
            builder.Logging.AddDebug();
#endif

            return builder.Build();
        }

        public static MauiAppBuilder RegisterAppServices(this MauiAppBuilder mauiAppBuilder)
        {
#if ANDROID
            mauiAppBuilder.Services.AddSingleton<ISensorHelper, SensorHelper>();
#endif
            return mauiAppBuilder;
        }
    }
}
