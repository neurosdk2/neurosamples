﻿namespace CallibriDemo.UWP;

public sealed partial class MainPage
{
    public MainPage()
    {
        InitializeComponent();

        LoadApplication(new CallibriDemo.App());
    }
}
